#version 330

uniform vec3 ambientColor;
uniform vec3 diffuseColor;
uniform vec3 specularColor;
uniform float specularExponent;
uniform float alpha;
uniform sampler2D sampler;

in vec2 fragmentTextureCoordinates;

out vec4 color;

/// @todo Implement Phong shader.
void main()
{
    // Pretend to use these uniforms to hide warnings
    diffuseColor; specularColor; specularExponent;

    // Just use ambient color for now and apply the texture
    color = vec4(ambientColor, alpha) * texture(sampler, fragmentTextureCoordinates);
}
