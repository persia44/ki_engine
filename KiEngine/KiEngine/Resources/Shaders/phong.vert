#version 330

uniform mat4 modelViewProjectionMatrix;

in vec3 vertexPosition;
in vec2 vertexTextureCoordinates;

out vec2 fragmentTextureCoordinates;

/// @todo Implement Phong shader.
void main()
{
    gl_Position = modelViewProjectionMatrix * vec4(vertexPosition, 1.0);
    fragmentTextureCoordinates = vertexTextureCoordinates;
}
