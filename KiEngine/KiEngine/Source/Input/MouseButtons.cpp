/**
 *  @file
 *      MouseButtons.cpp
 *
 *  @brief
 *      Defines mouse button codes.
 */

#include "MouseButtons.hpp"

#include <map>
#include <string>
#include <iostream>

/**
 *  @brief
 *      A mapping of each MouseButton to its corresponding string.
 */
static const std::map<const MouseButton, const std::string> mouseButtonStrings =
{
    {   MouseButton::One,       "1"         },
    {   MouseButton::Two,       "2"         },
    {   MouseButton::Three,     "3"         },
    {   MouseButton::Four,      "4"         },
    {   MouseButton::Five,      "5"         },
    {   MouseButton::Six,       "6"         },
    {   MouseButton::Seven,     "7"         },
    {   MouseButton::Eight,     "8"         },
    {   MouseButton::Last,      "LAST"      },
    {   MouseButton::Left,      "LEFT"      },
    {   MouseButton::Right,     "RIGHT"     },
    {   MouseButton::Middle,    "MIDDLE"    }
};

std::ostream& operator<<(std::ostream& stream, const MouseButton button)
{
    stream << mouseButtonStrings.at(button);
    return stream;
}
