/**
 *  @file
 *      MouseButtons.hpp
 *
 *  @brief
 *      Defines mouse button codes.
 */

#ifndef MouseButtons_hpp
#define MouseButtons_hpp

#include <iosfwd>

/**
 *  @brief
 *      Mouse buttons codes as defined by GLFW.
 */
enum class MouseButton
{
    One     = 0,
    Two     = 1,
    Three   = 2,
    Four    = 3,
    Five    = 4,
    Six     = 5,
    Seven   = 6,
    Eight   = 7,
    Last    = Eight,
    Left    = One,
    Right   = Two,
    Middle  = Three
};

/**
 *  @brief
 *      Inserts a MouseButton into an output stream.
 *
 *  @param [in, out] stream
 *      The output stream.
 *
 *  @param [in] button
 *      The MouseButton to insert.
 *
 *  @return
 *      \a stream.
 */
std::ostream& operator<<(std::ostream& stream, const MouseButton button);

#endif /* MouseButtons_hpp */
