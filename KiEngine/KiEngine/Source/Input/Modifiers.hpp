/**
 *  @file
 *      Modifiers.hpp
 *
 *  @brief
 *      Defines modifier key flags.
 */

#ifndef Modifiers_hpp
#define Modifiers_hpp

#include <iosfwd>

/**
 *  @brief
 *      Modifier key flags as defined by GLFW.
 *      None has been added for our own purposes.
 */
enum class Modifier
{
    /* This is not a real Modifier. It is simply used as a
     * placeholder when a Modifier is required */
    None    = 0x0000,
    /* If this bit is set one or more Shift keys were held down. */
    Shift   = 0x0001,
    /* If this bit is set one or more Control keys were held down. */
    Control = 0x0002,
    /* If this bit is set one or more Alt keys were held down. */
    Alt     = 0x0004,
    /* If this bit is set one or more Super keys were held down. */
    Super   = 0x0008
};

/**
 *  @brief
 *      Calculates the bitwise AND of two Modifiers.
 *
 *  @param [in] modifier1
 *      The first Modifier.
 *
 *  @param [in] modifier2
 *      The second Modifier.
 *
 *  @return
 *      The bitwise AND of \a modifier1 and \a modifier2.
 */
int operator&(const Modifier modifier1, const Modifier modifier2);

/**
 *  @brief
 *      Inserts a Modifier into an output stream.
 *
 *  @param [in, out] stream
 *      The output stream.
 *
 *  @param [in] modifier
 *      The Modifier to insert.
 *
 *  @return
 *      \a stream.
 */
std::ostream& operator<<(std::ostream& stream, const Modifier modifier);

#endif /* Modifiers_hpp */
