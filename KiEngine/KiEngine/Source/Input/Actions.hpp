/**
 *  @file
 *      Actions.hpp
 *
 *  @brief
 *      Defines input actions.
 */

#ifndef Actions_hpp
#define Actions_hpp

#include <iosfwd>

/**
 *  @brief
 *      Input actions as defined by GLFW
 */
enum class Action
{
    // The key or mouse button was released.
    Release = 0,
    // The key or mouse button was pressed.
    Press   = 1,
    // The key was held down until it repeated.
    Repeat  = 2,
};

/**
 *  @brief
 *      Inserts an Action into an output stream.
 *
 *  @param [in, out] stream
 *      The output stream.
 *
 *  @param [in] action
 *      The Action to insert.
 *
 *  @return
 *      \a stream.
 */
std::ostream& operator<<(std::ostream& stream, const Action action);

#endif /* Actions_hpp */
