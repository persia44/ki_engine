/**
 *  @file
 *      Modifiers.cpp
 *
 *  @brief
 *      Defines modifier key flags.
 */

#include "Modifiers.hpp"

#include <map>
#include <string>
#include <iostream>

/**
 *  @brief
 *      A mapping of each Modifier to its corresponding string.
 */
static const std::map<const Modifier, const std::string> modifierStrings =
{
    {   Modifier::Shift,    "SHIFT"     },
    {   Modifier::Control,  "CONTROL"   },
    {   Modifier::Alt,      "ALT"       },
    {   Modifier::Super,    "SUPER"     }
};

int operator&(const Modifier modifier1, const Modifier modifier2)
{
    return (int) modifier1 & (int) modifier2;
}

std::ostream& operator<<(std::ostream& stream, const Modifier modifier)
{
    KI_UNIMPLEMENTED_FUNCTION();

#if 0
    Modifier modifiers[4] = { Modifier::None };

    if (modifier & Modifier::Shift)
    {
        modifiers[0] = Modifier::Shift;
    }
    if (modifier & Modifier::Control)
    {
        modifiers[1] = Modifier::Control;
    }
    if (modifier & Modifier::Alt)
    {
        modifiers[2] = Modifier::Alt;
    }
    if (modifier & Modifier::Super)
    {
        modifiers[3] = Modifier::Super;
    }

    stream << modifiers;
#endif

    return stream;
}
