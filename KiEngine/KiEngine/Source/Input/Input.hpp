/**
 *  @file
 *      Input.hpp
 *
 *  @brief
 *      Contains input management code.
 */

#ifndef Input_hpp
#define Input_hpp

#include "Keys.hpp"
#include "MouseButtons.hpp"
#include "Actions.hpp"

#include "Vector.hpp"

/**
 *  @brief
 *      A namespace responsible for handling input.
 */
namespace Input
{
    /**
     *  @brief
     *      Updates input.
     */
    void Update();

    /**
     *  @brief
     *      Checks if a Key is currently pressed down.
     *
     *  @param [in] key
     *      The Key to query.
     *
     *  @retval true
     *      \a key is currently pressed down.
     *
     *  @retval false
     *      \a key is not currently pressed down.
     */
    bool IsPressed(Key key);

    /**
     *  @brief
     *      Checks if a MouseButton is currently pressed down.
     *
     *  @param [in] button
     *      The MouseButton to query.
     *
     *  @retval true
     *      \a button is currently pressed down.
     *
     *  @retval false
     *      \a button is not currently pressed down.
     */
    bool IsPressed(MouseButton button);

    /**
     *  @brief
     *      Gets the cursor's current position.
     *
     *  @return
     *      The coordinates of the cursor's current position.
     */
    Vec2f GetCursorPosition();

    /**
     *  @brief
     *      Gets the change in the cursor's position since the last frame.
     *
     *  @return
     *      The difference between the cursor's current and previous positions.
     */
    Vec2f GetMouseDelta();
}

#endif /* Input_hpp */
