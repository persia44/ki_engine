/**
 *  @file
 *      Actions.cpp
 *
 *  @brief
 *      Defines input actions.
 */

#include "Actions.hpp"

#include <map>
#include <string>
#include <iostream>

/**
 *  @brief
 *      A mapping of each Action to its corresponding string.
 */
static const std::map<const Action, const std::string> actionStrings =
{
    {   Action::Release,    "RELEASE"   },
    {   Action::Press,      "PRESS"     },
    {   Action::Repeat,     "REPEAT"    }
};

std::ostream& operator<<(std::ostream& stream, const Action action)
{
    stream << actionStrings.at(action);
    return stream;
}
