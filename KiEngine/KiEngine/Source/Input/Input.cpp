/**
 *  @file
 *      Input.cpp
 *
 *  @brief
 *      Contains input management code.
 */

#include "Input.hpp"

#include "Game.hpp"
#include "Window.hpp"

namespace Input
{

    /**
     *  @brief
     *      The change in the cursor's position since the last frame.
     */
    static Vec2f mouseDelta;

    void Update()
    {
        // Update the mouse delta
        static Vec2f lastCursorPosition = GetCursorPosition();
        Vec2f currentCursorPosition = GetCursorPosition();
        mouseDelta = currentCursorPosition - lastCursorPosition;
        lastCursorPosition = currentCursorPosition;
    }

    bool IsPressed(Key key)
    {
        return Game::Get().GetWindow().GetKeyState(key) == Action::Press;
    }

    bool IsPressed(MouseButton button)
    {
        return Game::Get().GetWindow().GetMouseButtonState(button) == Action::Press;
    }

    Vec2f GetCursorPosition()
    {
        double x, y;
        Game::Get().GetWindow().GetCursorPosition(x, y);
        return Vec2f(x, y);
    }

    Vec2f GetMouseDelta()
    {
        return mouseDelta;
    }

}
