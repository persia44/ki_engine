/**
 *  @file
 *      TestGame.hpp
 *
 *  @brief
 *      A test game.
 */

#ifndef Test_hpp
#define Test_hpp

#include "Game.hpp"

class TestGame : public Game
{
public:
    /**
     *  @brief
     *      Constructor.
     */
    TestGame();

protected:
    /**
     *  @brief
     *      Initializes the game.
     */
    void Initialize();

    /**
     *  @brief
     *      Updates the game.
     */
    void Update();

    /**
     *  @brief
     *      Renders the game.
     */
    void Render();

    /**
     *  @brief
     *      Terminates the game.
     */
    void Terminate();
};

#endif /* Test_hpp */
