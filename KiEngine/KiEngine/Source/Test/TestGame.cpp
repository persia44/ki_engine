/**
 *  @file
 *      TestGame.cpp
 *
 *  @brief
 *      A test game.
 */

#include "TestGame.hpp"

#include "Mesh.hpp"
#include "PhongShader.hpp"

#include "FirstPersonCamera.hpp"

Shader *shader;
Texture *white;
Texture *texture1;
Texture *texture2;
Material *groundMaterial;
Material *material1;
Material *material2;
Mesh *ground;
Mesh *cube1;
Mesh *cube2;

FirstPersonCamera camera;

TestGame::TestGame() :
    Game("Test Game")
{
}

void TestGame::Initialize()
{
    shader = new PhongShader();
    white = new Texture("White.png");
    texture1 = new Texture("Texture1.png");
    texture2 = new Texture("Texture2.png");

    groundMaterial = new Material(Vec3f(0.7), 1, *shader, *white);
    material1 = new Material(Vec3f(0.8, 0.2, 0.2), 1, *shader, *texture1);
    material2 = new Material(Vec3f(0.2, 0.2, 0.8), 1, *shader, *texture2);

    ground = new Mesh("Ground.obj", *groundMaterial, Vec3f(0, -0.5, 0));

    std::vector<Vertex> vertices =
    {
        Vertex(1, -1, -1, 0, 0),    // bottom-right-back
        Vertex(1, -1, 1, 1, 0),     // bottom-right-front
        Vertex(-1, -1, 1, 0, 0),    // bottom-left-front
        Vertex(-1, -1, -1, 0, 1),   // bottom-left-back
        Vertex(1, 1, -1, 0, 1),     // top-right-back
        Vertex(1, 1, 1, 1, 1),      // top-right-front
        Vertex(-1, 1, 1, 0, 1),     // top-left-front
        Vertex(-1, 1, -1, 1, 1)     // top-left-back
    };

    std::vector<int> indices =
    {
        1, 2, 3,
        7, 6, 5,
        4, 5, 1,
        5, 6, 2,
        2, 6, 7,
        0, 3, 7,
        0, 1, 3,
        4, 7, 5,
        0, 4, 1,
        1, 5, 2,
        3, 2, 7,
        4, 0, 7
    };

    cube1 = new Mesh(vertices, indices, *material1, Vec3f(-2, 2, 5));
    cube2 = new Mesh(vertices, indices, *material2, Vec3f(2, 1, 5));
}

void TestGame::Update()
{
    camera.Update();

    ground->SetViewProjectionMatrix(camera.GetViewProjectionMatrix());
    cube1->SetViewProjectionMatrix(camera.GetViewProjectionMatrix());
    cube2->SetViewProjectionMatrix(camera.GetViewProjectionMatrix());
}

void TestGame::Render()
{
    ground->Render();
    cube1->Render();
    cube2->Render();
}

void TestGame::Terminate()
{
    delete white;
    delete texture1;
    delete texture2;
    delete shader;
    delete material1;
    delete material2;
    delete ground;
    delete cube1;
    delete cube2;
}
