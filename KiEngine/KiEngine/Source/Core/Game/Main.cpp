/**
 *  @file
 *      Main.cpp
 *
 *  @brief
 *      Contains the main function.
 */

#include <cstdlib>

#include "TestGame.hpp"

/**
 *  @brief
 *      Starts the game.
 */
int main()
{
    TestGame().Start();

    return EXIT_SUCCESS;
}
