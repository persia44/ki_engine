/**
 *  @file
 *      Game.cpp
 *
 *  @brief
 *      Contains code for managing the game.
 */

#include "Game.hpp"

#include <string>

#include "Resource.hpp"

#include "Window.hpp"
#include "Input.hpp"
#include "Time.hpp"

static Game *game = nullptr;

static const int    FRAME_RATE_CAP  = 5000;
static const double FRAME_TIME      = 1 / static_cast<double>(FRAME_RATE_CAP);

Game& Game::Get()
{
    return *game;
}

Game::Game(std::string name) :
    window(name, 800, 600)
{
    game = this;
}

Game::~Game()
{
    game = nullptr;
}

void Game::Start()
{
    if (!isRunning)
    {
        isRunning = true;
        Run();
    }
}

void Game::Stop()
{
    isRunning = false;
}

Window& Game::GetWindow()
{
    return window;
}

void Game::Run()
{
    // Initialize the game
    Initialize();

    // Run the game loop
    while (isRunning)
    {
        // Update time
        Time::UpdateDeltaTime();
        timeToNextFrame -= Time::GetDeltaTime();

        // Poll for window events
        window.PollEvents();
        isRunning = !window.ShouldClose();

        // Update input
        Input::Update();

        // Update the game
        Update();

        if (timeToNextFrame <= 0)
        {
            // Render the game
            window.Clear();
            Render();
            window.SwapBuffers();

            // Update time
            timeToNextFrame = FRAME_TIME;
        }
    }

    // Terminate the game
    Terminate();
}
