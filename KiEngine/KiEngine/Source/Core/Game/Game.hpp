/**
 *  @file
 *      Game.hpp
 *
 *  @brief
 *      Contains the Game class.
 */

#ifndef Game_hpp
#define Game_hpp

#include <iosfwd>

#include "Window.hpp"

/**
 *  @brief
 *      A class responsible for managing the game.
 */
class Game
{
public:
    /**
     *  @brief
     *      Gets the current Game.
     *
     *  @return
     *      The Game.
     */
    static Game& Get();

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] name
     *      The game name. This is displayed as the title of the game window.
     */
    Game(std::string name);

    /**
     *  @brief
     *      Destructor.
     */
    virtual ~Game();

    /**
     *  @brief
     *      Starts the game.
     */
    void Start();

    /**
     *  @brief
     *      Stops the game.
     */
    void Stop();

    /**
     *  @brief
     *      Gets the game window.
     *
     *  @return
     *      The game window.
     */
    Window& GetWindow();

protected:
    /**
     *  @brief
     *      Initializes the game.
     */
    virtual void Initialize() = 0;

    /**
     *  @brief
     *      Updates the game.
     */
    virtual void Update() = 0;

    /**
     *  @brief
     *      Renders the game.
     */
    virtual void Render() = 0;

    /**
     *  @brief
     *      Terminates the game.
     */
    virtual void Terminate() = 0;

private:
    /**
     *  @brief
     *      The game window.
     */
    Window window;

    /**
     *  @brief
     *      Keeps track of the current run state of the game.
     */
    bool isRunning = false;

    /**
     *  @brief
     *      The time until the next frame.
     */
    double timeToNextFrame = 0;

    /**
     *  @brief
     *      Runs the game loop.
     */
    void Run();

};

#endif /* Game_hpp */
