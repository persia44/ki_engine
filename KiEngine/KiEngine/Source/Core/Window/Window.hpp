/**
 *  @file
 *      Window.hpp
 *
 *  @brief
 *      Contains a class for window management.
 */

#ifndef Window_hpp
#define Window_hpp

#include <string>

#include "Keys.hpp"
#include "MouseButtons.hpp"
#include "Modifiers.hpp"
#include "Actions.hpp"

/**
 *  @brief
 *      Forward declaration.
 */
struct GLFWwindow;

/**
 *  @brief
 *      A class responsible for managing a window.
 */
class Window
{
public:
    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] title
     *      The window title.
     *
     *  @param [in] width
     *      The window width.
     *
     *  @param [in] height
     *      The window height.
     *
     *  Creates and opens a window.
     */
    Window(std::string title, int width, int height);

    /**
     *  @brief
     *      Destructor.
     *
     *  Closes and destroys the window.
     */
    ~Window();

    /**
     *  @brief
     *      Polls for window events.
     */
    void PollEvents();

    /**
     *  @brief
     *      Clears the window.
     */
    void Clear();

    /**
     *  @brief
     *      Swaps the draw buffers.
     */
    void SwapBuffers();

    /**
     *  @brief
     *      Gets the size of the window, in pixels.
     *
     *  @param [out] width
     *      The width of the window.
     *
     *  @param [out] height
     *      The height of the window.
     */
    void GetResolution(int& width, int& height);

    /**
     *  @brief
     *      Finds the aspect ratio.
     *
     *  @return
     *      The aspect ratio.
     */
    double GetAspectRatio();

    /**
     *  @brief
     *      Checks if the window should close.
     *
     *  @retval true
     *      The window should close.
     *
     *  @retval false
     *      The window does not need to close.
     */
    bool ShouldClose();

    /**
     *  @brief
     *      Gets the current state of a key.
     *
     *  @param [in] key
     *      The key to query.
     *
     *  @retval Press
     *      \a key is currently pressed.
     *
     *  @retval Release
     *      \a key is currently not pressed.
     */
    Action GetKeyState(Key key);

    /**
     *  @brief
     *      Gets the current state of a mouse button.
     *
     *  @param [in] button
     *      The mouse button to query.
     *
     *  @retval Press
     *      \a button is currently pressed.
     *
     *  @retval Release
     *      \a button is currently not pressed.
     */
    Action GetMouseButtonState(MouseButton button);

    /**
     *  @brief
     *      Gets the current position of the cursor.
     *
     *  @param [out] x
     *      The x coordinate of the cursor.
     *
     *  @param [out] y
     *      The y coordinate of the cursor.
     */
    void GetCursorPosition(double& x, double& y);

private:
    /**
     *  @brief
     *      The window.
     */
    GLFWwindow *window = nullptr;

    /**
     *  @brief
     *      The window title.
     */
    const std::string title;

    /**
     *  @brief
     *      The window width.
     */
    int width;

    /**
     *  @brief
     *      The window height.
     */
    int height;
};

#endif /* Window_hpp */
