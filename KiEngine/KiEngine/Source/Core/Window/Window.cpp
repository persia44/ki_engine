/**
 *  @file
 *      Window.cpp
 *
 *  @brief
 *      Contains a class for window management.
 */

#include "Window.hpp"

#include <GLFW/glfw3.h>
#include <string>

#include "Render.hpp"

Window::Window(std::string title, int width, int height) :
    title(title),
    width(width),
    height(height)
{
    // Initialize GLFW
    if (!glfwInit())
    {
        return;
    }

    // Set up OpenGL
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    // Create the window
    window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
    if (!window)
    {
        return;
    }

    // Disable the cursor
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // Make the OpenGL context current
    glfwMakeContextCurrent(window);

    // Initialize the rendering system
    Render::Initialize();
}

Window::~Window()
{
    // Destroy the window
    glfwDestroyWindow(window);

    /// @todo Do this somewhere else; this will destroy *all* windows (maybe also init GLFW somewhere else too)
    // Terminate GLFW
    glfwTerminate();
}

void Window::PollEvents()
{
    glfwPollEvents();
}

void Window::Clear()
{
    Render::Clear();
}

void Window::SwapBuffers()
{
    glfwSwapBuffers(window);
}

void Window::GetResolution(int& width, int& height)
{
    glfwGetWindowSize(window, &width, &height);
}

double Window::GetAspectRatio()
{
    int width, height;
    GetResolution(width, height);

    return (double) width / (double) height;
}

bool Window::ShouldClose()
{
    return glfwWindowShouldClose(window);
}

Action Window::GetKeyState(Key key)
{
    return (Action) glfwGetKey(window, (int) key);
}

Action Window::GetMouseButtonState(MouseButton button)
{
    return (Action) glfwGetMouseButton(window, (int) button);
}

void Window::GetCursorPosition(double& x, double& y)
{
    glfwGetCursorPos(window, &x, &y);
}
