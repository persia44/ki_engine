/**
 *  @file
 *      Time.cpp
 *
 *  @brief
 *      Contains functions related to time.
 */

#include "Time.hpp"

#include <chrono>
#include <sstream>
#include <iomanip>

using namespace std::chrono;

namespace Time
{

    /**
     *  @brief
     *      The time at which the program started.
     */
    static const steady_clock::time_point startTimePoint = steady_clock::now();

    /**
     *  @brief
     *      The time of the last frame.
     */
    static steady_clock::time_point lastFrameTimePoint = startTimePoint;

    /**
     *  @brief
     *      The time since the last frame.
     */
    static steady_clock::duration deltaTime = steady_clock::duration::zero();

    /**
     *  @brief
     *      Converts a steady_clock::duration to a double.
     *
     *  @param [in] duration
     *      The duration to convert.
     *
     *  @param [in] precision
     *      The precision to return the time in.
     *
     *  @returns
     *      A double representing \a duration in the given precision.
     */
    static double DurationToDouble(steady_clock::duration duration, TimeUnit precision);

    double GetCurrentTime(TimeUnit precision)
    {
        return DurationToDouble(steady_clock::now() - startTimePoint, precision);
    }

    std::string CurrentTimeToString(TimeUnit precision)
    {
        std::ostringstream output;
        output << std::setfill('0');

        steady_clock::duration currentTime = steady_clock::now() - startTimePoint;

        if (precision >= Hours)
        {
            auto hours = duration_cast<std::chrono::hours>(currentTime);
            currentTime -= hours;
            output << hours.count();
        }
        if (precision >= Minutes)
        {
            auto minutes = duration_cast<std::chrono::minutes>(currentTime);
            currentTime -= minutes;
            output << ":" << std::setw(2) << std::to_string(minutes.count());
        }
        if (precision >= Seconds)
        {
            auto seconds = duration_cast<std::chrono::seconds>(currentTime);
            currentTime -= seconds;
            output << ":" << std::setw(2) << std::to_string(seconds.count());
        }
        if (precision >= Milliseconds)
        {
            auto milliseconds = duration_cast<std::chrono::milliseconds>(currentTime);
            currentTime -= milliseconds;
            output << ":" << std::setw(3) << std::to_string(milliseconds.count());
        }
        if (precision >= Microseconds)
        {
            auto microseconds = duration_cast<std::chrono::microseconds>(currentTime);
            currentTime -= microseconds;
            output << ":" << std::setw(3) << std::to_string(microseconds.count());
        }
        if (precision >= Nanoseconds)
        {
            auto nanoseconds = duration_cast<std::chrono::nanoseconds>(currentTime);
            output << ":" << std::setw(3) << std::to_string(nanoseconds.count());
        }

        return output.str();
    }

    double GetDeltaTime(TimeUnit precision)
    {
        return DurationToDouble(deltaTime, precision);
    }

    void UpdateDeltaTime()
    {
        steady_clock::time_point thisFrameTimePoint = steady_clock::now();
        deltaTime = thisFrameTimePoint - lastFrameTimePoint;
        lastFrameTimePoint = thisFrameTimePoint;
    }

    double DurationToDouble(steady_clock::duration duration, TimeUnit precision)
    {
        switch (precision)
        {
            case Hours:
                return duration_cast<hours>(duration).count();

            case Minutes:
                return duration_cast<minutes>(duration).count();

            case Seconds:
                return duration_cast<seconds>(duration).count();

            case Milliseconds:
                return duration_cast<milliseconds>(duration).count();

            case Microseconds:
                return duration_cast<microseconds>(duration).count();

            case Nanoseconds:
                return duration_cast<nanoseconds>(duration).count();
        }
    }

}
