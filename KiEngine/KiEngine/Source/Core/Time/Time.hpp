/**
 *  @file
 *      Time.hpp
 *
 *  @brief
 *      Contains functions related to time.
 */

#ifndef Time_hpp
#define Time_hpp

#include <iosfwd>

/**
 *  @brief
 *      Namespace used for querying time.
 */
namespace Time
{
    /**
     *  @brief
     *      Represents units of time.
     */
    enum TimeUnit
    {
        Hours,
        Minutes,
        Seconds,
        Milliseconds,
        Microseconds,
        Nanoseconds
    };

    /**
     *  @brief
     *      Gets the current time.
     *
     *  @param [in] precision
     *      The precision to return the time in. Defaults to Milliseconds.
     *
     *  @returns
     *      The current time in the given precision.
     */
    double GetCurrentTime(TimeUnit precision = Milliseconds);

    /**
     *  @brief
     *      Gets the current time formatted in a string.
     *
     *  @param [in] precision
     *      The precision to return the time in. Defaults to Milliseconds.
     *
     *  @return
     *      A string representing the current time in the given precision.
     */
    std::string CurrentTimeToString(TimeUnit precision = Milliseconds);

    /**
     *  @brief
     *      Gets the time since the last frame.
     *
     *  @param [in] precision
     *      The precision to return the time in. Defaults to Milliseconds.
     *
     *  @returns
     *      The delta time in the given precision.
     */
    double GetDeltaTime(TimeUnit precision = Milliseconds);

    /**
     *  @brief
     *      Updates the delta time.
     */
    void UpdateDeltaTime();
};

#endif /* Time_hpp */
