/**
 *  @file
 *      Image.hpp
 *
 *  @brief
 *      Contains an Image class.
 */

#include "Image.hpp"

#include <png.h>
#include <string>

Image::Image(std::string filename) :
    File(filename)
{
    // Check the image file extension to see how to handle it
    std::string extension = GetExtension(filename);

    if (extension == "png")
    {
        LoadPNG(filename);
    }
    else
    {
        KI_ERROR("Could not read the image \"" << filename << "\"; file format not supported");
    }
}

Image::~Image()
{
    delete[] data;
}

unsigned char *Image::GetData()
{
    return data;
}

int Image::GetWidth()
{
    return width;
}

int Image::GetHeight()
{
    return height;
}

bool Image::HasAlpha()
{
    return hasAlpha;
}

void Image::LoadPNG(std::string filename)
{
    /**
     *  @todo
     *      Use File functions to read from the file instead of the C functions.
     */

    // Open the image file
    FILE *file = fopen(filename.c_str(), "rb");
    if (!file)
    {
        KI_ERROR("Failed to open image \"" << filename << "\"");

        return;
    }

    // Read the header to double check for PNG format
    const size_t signatureBytes = 8;
    char header[signatureBytes];
    fread(header, 1, signatureBytes, file);
    if (png_sig_cmp((png_const_bytep) header, 0, signatureBytes))
    {
        KI_ERROR("The image \"" << filename << "\" could not be read; file corrupted");
        fclose(file);

        return;
    }

    // Create png_struct
    png_structp pngPtr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    if (!pngPtr)
    {
        KI_ERROR("The image \"" << filename << "\" could not be read");
        fclose(file);

        return;
    }

    // Create png_info
    png_infop infoPtr = png_create_info_struct(pngPtr);
    if (!infoPtr)
    {
        KI_ERROR("The image \"" << filename << "\" could not be read");
        png_destroy_read_struct(&pngPtr, nullptr, nullptr);
        fclose(file);

        return;
    }

    // Create png_info
    png_infop endInfo = png_create_info_struct(pngPtr);
    if (!endInfo)
    {
        KI_ERROR("The image \"" << filename << "\" could not be read");
        png_destroy_read_struct(&pngPtr, &infoPtr, nullptr);
        fclose(file);

        return;
    }

    // Check for errors
    if (setjmp(png_jmpbuf(pngPtr)))
    {
        png_destroy_read_struct(&pngPtr, &infoPtr, nullptr);
        fclose(file);

        return;
    }

    // Initialize PNG IO
    png_init_io(pngPtr, file);

    // Specify that we have already read the header
    png_set_sig_bytes(pngPtr, signatureBytes);

    // Read all the information up to the image data
    png_read_info(pngPtr, infoPtr);

    // Read information about the image
    int bitDepth, colorType;
    png_get_IHDR(pngPtr,
                 infoPtr,
                 (png_uint_32p) &width,
                 (png_uint_32p) &height,
                 &bitDepth,
                 &colorType,
                 nullptr,
                 nullptr,
                 nullptr);
    hasAlpha = colorType & PNG_COLOR_MASK_ALPHA;

    // Update the png_info
    png_read_update_info(pngPtr, infoPtr);

    // Get the row size
    unsigned long rowBytes = png_get_rowbytes(pngPtr, infoPtr);

    // Allocate memory to store the image data
    data = new unsigned char[rowBytes * height];
    if (!data)
    {
        png_destroy_read_struct(&pngPtr, &infoPtr, &endInfo);
        fclose(file);

        return;
    }

    // Get pointers to each row of the image data
    png_bytep rowPointers[height];
    for (int i = 0; i < height; i++)
    {
        rowPointers[height - i - 1] = &data[i * rowBytes];
    }

    // Read the image
    png_read_image(pngPtr, rowPointers);

    // Clean up
    png_destroy_read_struct(&pngPtr, &infoPtr, &endInfo);
    fclose(file);
}
