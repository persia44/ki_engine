/**
 *  @file
 *      File.cpp
 *
 *  @brief
 *      Contains a File class.
 */

#include "File.hpp"

#include <string>

std::string File::GetExtension(std::string filename)
{
    size_t lastPeriodPosition = filename.find_last_of('.');

    if (lastPeriodPosition == filename.npos)
    {
        return "";
    }
    else
    {
        return filename.substr(lastPeriodPosition + 1);
    }
}

File::File(std::string filename) :
    filename(filename)
{
    file = std::ifstream(filename, std::ios::binary);

    if (!file)
    {
        KI_ERROR("Failed to open file " << filename);
    }
}

File::File(const File& file) :
    File(file.filename)
{
}

File::~File()
{
    if (file)
    {
        file.close();
    }
}

std::string File::GetFilename() const
{
    return filename;
}

std::string File::GetContents()
{
    std::string contents;

    if (file)
    {
        // Set the string size to the file size
        file.seekg(0, file.end);
        contents.resize(file.tellg());
        file.seekg(0);

        // Read the file into the string
        file.read(&contents[0], contents.size());
        file.close();

        return contents;
    }

    return contents;
}

std::string File::GetLine()
{
    std::string line;
    GetLine(line);
    return line;
}

bool File::GetLine(std::string& string)
{
    std::getline(file, string);
    return !file.fail();
}
