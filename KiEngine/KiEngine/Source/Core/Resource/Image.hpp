/**
 *  @file
 *      Image.hpp
 *
 *  @brief
 *      Contains an Image class.
 */

#ifndef Image_hpp
#define Image_hpp

#include <iosfwd>

#include "File.hpp"

/**
 *  @brief
 *      A class used for loading an image file.
 */
class Image : File
{
public:
    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] filename
     *      The name of the image file to load.
     */
    Image(std::string filename);

    /**
     *  @brief
     *      Destructor.
     */
    ~Image();

    /**
     *  @brief
     *      Gets the image data.
     *
     *  @return
     *      \a data.
     */
    unsigned char *GetData();

    /**
     *  @brief
     *      Gets the image width.
     *
     *  @return
     *      \a width.
     */
    int GetWidth();

    /**
     *  @brief
     *      Gets the image height.
     *
     *  @return
     *      \a height.
     */
    int GetHeight();

    /**
     *  @brief
     *      Checks if the image has alpha or not.
     *
     *  @retval true
     *      The image has alpha.
     *
     *  @retval false
     *      The image does not have alpha.
     */
    bool HasAlpha();

private:
    /**
     *  @brief
     *      The image data.
     */
    unsigned char *data = nullptr;

    /**
     *  @brief
     *      The image width.
     */
    int width;

    /**
     *  @brief
     *      The image height.
     */
    int height;

    /**
     *  @brief
     *      Specifies if the image has alpha or not.
     */
    bool hasAlpha;

    /**
     *  @brief
     *      Loads a Portable Network Graphics (PNG) image.
     */
    void LoadPNG(std::string filename);
};

#endif /* Image_hpp */
