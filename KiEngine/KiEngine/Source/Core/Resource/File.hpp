/**
 *  @file
 *      File.hpp
 *
 *  @brief
 *      Contains a File class.
 */

#ifndef File_hpp
#define File_hpp

#include <iosfwd>
#include <fstream>

/**
 *  @brief
 *      A class used for loading a file.
 */
class File
{
public:
    /**
     *  @brief
     *      Finds the file extension of a file.
     *
     *  @param [in] filename
     *      The name of the file to query.
     *
     *  @return
     *      If found, returns the file extension.
     *      Otherwise, returns an empty string ("").
     */
    static std::string GetExtension(std::string filename);

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] filename
     *      The name of the file to open.
     */
    File(std::string filename);

    /**
     *  @brief
     *      Copy constructor.
     *
     *  @param [in] file
     *      The File to copy from.
     */
    File(const File& file);

    /**
     *  @brief
     *      Destructor.
     */
    ~File();

    /**
     *  @brief
     *      Gets the filename.
     *
     *  @return
     *      The filename.
     */
    std::string GetFilename() const;

    /**
     *  @brief
     *      Gets the contents of the File.
     *
     *  @return
     *      The File contents.
     */
    std::string GetContents();

    /**
     *  @brief
     *      Extracts a line from the File.
     *
     *  @return
     *      The next line in the File.
     */
    std::string GetLine();

    /**
     *  @brief
     *      Extracts a line from the File and stores it in a string.
     *
     *  @retval true
     *      The line was extracted and stored in \a string successfully.
     *
     *  @retval false
     *      The line failed to be extracted.
     */
    bool GetLine(std::string& string);

    /**
     *  @brief
     *      Extracts data from a File.
     *
     *  @param [in, out] variable
     *      The variable to insert the data into.
     *
     *  @return
     *      The File being extracted from.
     */
    template <typename T>
    File& operator>>(T& variable)
    {
        file >> variable;

        return *this;
    }

private:
    /**
     *  @brief
     *      The name of the file.
     */
    std::string filename;

    /**
     *  @brief
     *      The file stream.
     */
    std::ifstream file;
};

#endif /* File_hpp */
