/**
 *  @file
 *      Resource.hpp
 *
 *  @brief
 *      Contains functions for loading resources from files.
 */

#ifndef Resource_hpp
#define Resource_hpp

#include <iosfwd>

#include "File.hpp"
#include "Image.hpp"

/**
 *  @brief
 *      Manages resource files.
 */
namespace Resource
{

    /**
     *  @brief
     *      Resource types.
     */
    enum ResourceType
    {
        Material,
        Mesh,
        Shader,
        Texture
    };

    /**
     *  @brief
     *      Loads a resource.
     *
     *  @param [in] type
     *      The type of resource to load.
     *
     *  @param [in] filename
     *      The name of the file to load.
     *
     *  @return
     *      The file.
     */
    File Load(ResourceType type, std::string filename);

    /**
     *  @brief
     *      Loads a texture image.
     *
     *  @param [in] filename
     *      The name of the texture file to load.
     *
     *  @return
     *      The texture image.
     */
    Image LoadTexture(std::string filename);

}

#endif /* Resource_hpp */
