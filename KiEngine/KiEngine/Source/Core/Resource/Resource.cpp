/**
 *  @file
 *      Resource.hpp
 *
 *  @brief
 *      Contains functions for loading resources from files.
 */

#include "Resource.hpp"

#include <string>
#include <map>

namespace Resource
{

    /**
     *  @brief
     *      A mapping of each ResourceType to its corresponding location.
     */
    static std::map<const ResourceType, std::string> resourcePaths =
    {
        {   Material,   "Resources/Materials/"    },
        {   Mesh,       "Resources/Meshes/"       },
        {   Shader,     "Resources/Shaders/"      },
        {   Texture,    "Resources/Textures/"     }
    };

    File Load(ResourceType type, std::string filename)
    {
        return File(resourcePaths[type] + filename);
    }

    Image LoadTexture(std::string filename)
    {
        return Image(resourcePaths[Texture] + filename);
    }

}
