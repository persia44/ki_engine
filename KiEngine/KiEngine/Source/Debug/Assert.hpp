/**
 *  @file
 *      Assert.hpp
 *
 *  @brief
 *      Contains code for debug output and assertions.
 */

#ifndef Assert_hpp
#define Assert_hpp

#if DEBUG

    #include <iosfwd>
    #include <sstream>

    /**
     *  @brief
     *      Various functions which handle the output of debug messages.
     *
     *  These functions should not be called directly; use the KI_* macros instead.
     */
    /// @{
    void KiLog(std::string file, int line, std::string function,
               std::stringstream message);
    void KiWarning(std::string file, int line, std::string function,
                   std::stringstream message);
    void KiError(std::string file, int line, std::string function,
                 std::stringstream message);
    void KiAssert(std::string file, int line, std::string function,
                  std::string conditionString, bool conditionValue,
                  std::stringstream message);
    void KiUnimplementedFunction(std::string file, int line, std::string function);
    /// @}

    /**
     *  @brief
     *      Logs a message to the standard log stream.
     *
     *  @param [in] message
     *      The log message to output.
     */
    #define KI_LOG(message) \
        KiLog(__FILE__, __LINE__, __FUNCTION__, std::stringstream() << message)

    /**
     *  @brief
     *      Outputs a warning to the standard error stream.
     *
     *  @param [in] message
     *      The warning message to output.
     */
    #define KI_WARNING(message) \
        KiWarning(__FILE__, __LINE__, __FUNCTION__, std::stringstream() << message)

    /**
     *  @brief
     *      Outputs an error to the standard error stream.
     *
     *  @param [in] message
     *      The error message to output.
     */
    #define KI_ERROR(message) \
        KiError(__FILE__, __LINE__, __FUNCTION__, std::stringstream() << message)

    /**
     *  @brief
     *      Outputs an error if a condition fails.
     *
     *  @param [in] condition
     *      The condition to check.
     *
     *  @param [in] message
     *      The message to output.
     *
     *  If \a condition evaluates to true, nothing happens.
     *  If \a condition evaluates to false, an error is output.
     */
    #define KI_ASSERT(condition, message) \
        KiAssert(__FILE__, __LINE__, __FUNCTION__, #condition, condition, std::stringstream() << message)

    /**
     *  @brief
     *      Output a warning about the calling function being unimplemented.
     */
    #define KI_UNIMPLEMENTED_FUNCTION() \
        KiUnimplementedFunction(__FILE__, __LINE__, __FUNCTION__)

#else

    /**
     *  @brief
     *      A no operation expression.
     */
    #define KI_NOOP \
        ((void) 0)

    /**
     *  @name
     *      All debug macros are disabled in release mode.
     */
    /// @{
    #define KI_LOG(message) \
        KI_NOOP

    #define KI_WARNING(message) \
        KI_NOOP

    #define KI_ERROR(message) \
        KI_NOOP

    #define KI_ASSERT(condition, message) \
        KI_NOOP

    #define KI_UNIMPLEMENTED_FUNCTION() \
        KI_NOOP
    /// @}

#endif

#endif /* Assert_hpp */
