/**
 *  @file
 *      Assert.cpp
 *
 *  @brief
 *      Contains code for debug output and assertions.
 */

#include "Assert.hpp"

#include <string>
#include <sstream>
#include <iostream>

#include "Time.hpp"

/**
 *  @brief
 *      Creates a formatted string containing the current time and a given file location.
 *
 *  @param [in] file
 *      The file name.
 *
 *  @param [in] line
 *      The line number.
 *
 *  @param [in] function
 *      The function name.
 *
 *  @return
 *      A string formatted as "[currentTime] [filename:line function] ", where
 *      filename is \a file without the full path.
 */
static std::string KiMessagePrefix(std::string file, int line, std::string function)
{
    std::string output;

    unsigned long lastSlash = file.find_last_of('/');
    std::string filename = lastSlash == file.npos ? file : file.substr(lastSlash + 1);

    output += "[" + Time::CurrentTimeToString() + "] ";
    output += "[" + filename + ":" + std::to_string(line) + " " + function + "] ";

    return output;
}

void KiLog(std::string file, int line, std::string function,
           std::stringstream message)
{
    std::clog << KiMessagePrefix(file, line, function)
              << message.str() << std::endl;
}

void KiWarning(std::string file, int line, std::string function,
               std::stringstream message)
{
    std::cerr << KiMessagePrefix(file, line, function) << "[Warning] "
              << message.str() << std::endl;
}

void KiError(std::string file, int line, std::string function,
             std::stringstream message)
{
    std::cerr << KiMessagePrefix(file, line, function) << "[Error] "
              << message.str() << std::endl;
}

void KiAssert(std::string file, int line, std::string function,
              std::string conditionString, bool conditionValue,
              std::stringstream message)
{
    if (!conditionValue)
    {
        KiError(file, line, function,
                std::stringstream() << "Assertion failed: " << conditionString
                                    << " -- " << message.str());
    }
}

void KiUnimplementedFunction(std::string file, int line, std::string function)
{
    KiWarning(file, line, function, std::stringstream() << "Unimplemented function");
}
