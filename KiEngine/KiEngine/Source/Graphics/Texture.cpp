/**
 *  @file
 *      Texture.cpp
 *
 *  @brief
 *      Contains a Texture class.
 */

#include "Texture.hpp"

#include <GL/glew.h>
#include <string>

#include "Resource.hpp"
#include "Image.hpp"

Texture::Texture(std::string filename)
{
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    Image image = Resource::LoadTexture(filename);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 image.HasAlpha() ? GL_RGBA : GL_RGB,
                 image.GetWidth(),
                 image.GetHeight(),
                 0,
                 image.HasAlpha() ? GL_RGBA : GL_RGB,
                 GL_UNSIGNED_BYTE,
                 image.GetData());

    glBindTexture(GL_TEXTURE_2D, 0);
}

Texture::~Texture()
{
    glDeleteTextures(1, &texture);
}

void Texture::Use()
{
    glBindTexture(GL_TEXTURE_2D, texture);
}
