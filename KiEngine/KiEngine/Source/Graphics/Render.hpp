/**
 *  @file
 *      Render.hpp
 *
 *  @brief
 *      Contains functions related to rendering graphics.
 */

#ifndef Render_hpp
#define Render_hpp

/**
 *  @brief
 *      A namespace responsible for managing the rendering system.
 */
namespace Render
{
    /**
     *  @brief
     *      Initializes the rendering system.
     */
    void Initialize();

    /**
     *  @brief
     *      Clears the screen.
     */
    void Clear();
}

#endif /* Render_hpp */
