/**
 *  @file
 *      Shader.cpp
 *
 *  @brief
 *      Contains a Shader class for loading shaders.
 */

#include "Shader.hpp"

#include <GL/glew.h>
#include <string>

#include "Resource.hpp"
#include "File.hpp"

Shader::Shader(std::string vertexShaderFilename, std::string fragmentShaderFilename)
{
    // Create the shader program
    program = glCreateProgram();

    // Load the shaders
    vertexShader  = LoadShader(vertexShaderFilename);
    fragmentShader = LoadShader(fragmentShaderFilename);

    // Attach the shaders
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);

    // Link the program
    glLinkProgram(program);

    // Check the link status
    GLint status;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (!status)
    {
        GLsizei logLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
        GLchar *log = new GLchar[logLength];
        glGetProgramInfoLog(program, logLength, nullptr, log);
        KI_ERROR("Failed to link program:\n" << log);
        delete[] log;

        return;
    }

    // Register the modelViewProjectionMatrix uniform
    RegisterUniform("modelViewProjectionMatrix");

    // Register the color uniforms
    RegisterUniform("ambientColor");
    RegisterUniform("diffuseColor");
    RegisterUniform("specularColor");
    RegisterUniform("specularExponent");
    RegisterUniform("alpha");
}

Shader::~Shader()
{
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    glDeleteProgram(program);
}

void Shader::Use()
{
    glUseProgram(program);
}

int Shader::GetUniform(std::string uniform)
{
    auto uniformLocation = uniforms.find(uniform);
    if (uniformLocation != uniforms.end())
    {
        return uniformLocation->second;
    }
    else
    {
        KI_WARNING("Uniform \"" << uniform << "\" does not exist");
        return -1;
    }
}

int Shader::GetAttribute(std::string attribute)
{
    auto attributeLocation = attributes.find(attribute);
    if (attributeLocation != attributes.end())
    {
        return attributeLocation->second;
    }
    else
    {
        KI_WARNING("attribute \"" << attribute << "\" does not exist");
        return -1;
    }
}

void Shader::SetModelViewProjectionMatrix(Mat4x4f modelViewProjectionMatrix)
{
    SetUniform("modelViewProjectionMatrix", modelViewProjectionMatrix);
}

void Shader::SetAmbientColor(Vec3f ambientColor)
{
    SetUniform("ambientColor", ambientColor);
}

void Shader::SetDiffuseColor(Vec3f diffuseColor)
{
    SetUniform("diffuseColor", diffuseColor);
}

void Shader::SetSpecularColor(Vec3f specularColor)
{
    SetUniform("specularColor", specularColor);
}

void Shader::SetSpecularExponent(Vec3f specularExponent)
{
    SetUniform("specularExponent", specularExponent);
}

void Shader::SetAlpha(float alpha)
{
    SetUniform("alpha", alpha);
}

void Shader::RegisterUniform(std::string uniform)
{
    GLint uniformLocation = glGetUniformLocation(program, uniform.c_str());
    if (uniformLocation == -1)
    {
        KI_WARNING("Could not locate uniform \"" << uniform << "\"");
    }
    else
    {
        uniforms[uniform] = uniformLocation;
    }
}

void Shader::RegisterAttribute(std::string attribute)
{
    GLint attributeLocation = glGetAttribLocation(program, attribute.c_str());
    if (attributeLocation == -1)
    {
        KI_WARNING("Could not locate attribute \"" << attribute << "\"");
    }
    else
    {
        attributes[attribute] = attributeLocation;
    }
}

void Shader::SetUniform(std::string uniform, float value)
{
    int uniformLocation = GetUniform(uniform);
    glUniform1f(uniformLocation, value);
}

void Shader::SetUniform(std::string uniform, const Vec2f& value)
{
    int uniformLocation = GetUniform(uniform);
    glUniform2f(uniformLocation, value.x, value.y);
}

void Shader::SetUniform(std::string uniform, const Vec3f& value)
{
    int uniformLocation = GetUniform(uniform);
    glUniform3f(uniformLocation, value.x, value.y, value.z);
}

void Shader::SetUniform(std::string uniform, const Vec4f& value)
{
    int uniformLocation = GetUniform(uniform);
    glUniform4f(uniformLocation, value.x, value.y, value.z, value.w);
}

void Shader::SetUniform(std::string uniform, int value)
{
    int uniformLocation = GetUniform(uniform);
    glUniform1i(uniformLocation, value);
}

void Shader::SetUniform(std::string uniform, const Vec2i& value)
{
    int uniformLocation = GetUniform(uniform);
    glUniform2i(uniformLocation, value.x, value.y);
}

void Shader::SetUniform(std::string uniform, const Vec3i& value)
{
    int uniformLocation = GetUniform(uniform);
    glUniform3i(uniformLocation, value.x, value.y, value.z);
}

void Shader::SetUniform(std::string uniform, const Vec4i& value)
{
    int uniformLocation = GetUniform(uniform);
    glUniform4i(uniformLocation, value.x, value.y, value.z, value.w);
}

void Shader::SetUniform(std::string uniform, const Mat3x3f& value)
{
    int uniformLocation = GetUniform(uniform);
    glUniformMatrix3fv(uniformLocation, 1, GL_TRUE, value.elements);
}

void Shader::SetUniform(std::string uniform, const Mat4x4f& value)
{
    int uniformLocation = GetUniform(uniform);
    glUniformMatrix4fv(uniformLocation, 1, GL_TRUE, value.elements);
}

unsigned int Shader::LoadShader(std::string filename)
{
    // Use the file extension to determine the type of shader
    int shaderType;
    std::string extension = File::GetExtension(filename);

    if (extension == "vert")
    {
        shaderType = GL_VERTEX_SHADER;
    }
    else if (extension == "frag")
    {
        shaderType = GL_FRAGMENT_SHADER;
    }
    else
    {
        KI_ERROR("Failed to load shader \"" << filename << "\"; file format not recognised");
        return -1;
    }

    // Create the shader
    unsigned int shader = glCreateShader(shaderType);

    // Load the shader source
    std::string shaderContents = Resource::Load(Resource::Shader, filename).GetContents();
    const GLchar *shaderSource = shaderContents.c_str();
    glShaderSource(shader, 1, &shaderSource, nullptr);

    // Compile the shader
    glCompileShader(shader);
    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if (!status)
    {
        // Get the log
        GLsizei logLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
        GLchar *log = new GLchar[logLength];
        glGetShaderInfoLog(shader, logLength, nullptr, log);

        // Output an error message
        KI_ERROR("Failed to compile shader \"" << filename << "\":\n" << log);
        delete[] log;
    }

    return shader;
}
