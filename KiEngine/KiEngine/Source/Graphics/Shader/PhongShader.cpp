/**
 *  @file
 *      PhongShader.cpp
 *
 *  @brief
 *      Contains a PhongShader class.
 */

#include "PhongShader.hpp"

PhongShader::PhongShader() :
    Shader("phong.vert", "phong.frag")
{
    RegisterAttribute("vertexPosition");
    RegisterAttribute("vertexTextureCoordinates");
}
