/**
 *  @file
 *      PhongShader.hpp
 *
 *  @brief
 *      Contains a PhongShader class.
 */

#ifndef PhongShader_hpp
#define PhongShader_hpp

#include "Shader.hpp"

/**
 *  @brief
 *      A Phong Shader.
 */
class PhongShader : public Shader
{
public:
    /**
     *  @brief
     *      Constructor.
     */
    PhongShader();
};

#endif /* PhongShader_hpp */
