/**
 *  @file
 *      Shader.hpp
 *
 *  @brief
 *      Contains a Shader class for loading shaders.
 */

#ifndef Shader_hpp
#define Shader_hpp

#include <iosfwd>
#include <map>

#include "Vector.hpp"
#include "Matrix.hpp"

/**
 *  @brief
 *      A Shader is added to a Material to modify its appearance.
 */
class Shader
{
public:
    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] vertexShaderFilename
     *      The filename of the vertex shader to load.
     *
     *  @param [in] fragmentShaderFilename
     *      The filename of the fragment shader to load.
     */
    Shader(std::string vertexShaderFilename, std::string fragmentShaderFilename);

    /**
     *  @brief
     *      Destructor.
     */
    virtual ~Shader();

    /**
     *  @brief
     *      Use the Shader.
     *
     *  Only one Shader may be in use at any time.
     */
    void Use();

    /**
     *  @brief
     *      Gets the location of a uniform.
     *
     *  @param [in] uniform
     *      The name of the uniform.
     *
     *  @return
     *      The location of the uniform.
     */
    int GetUniform(std::string uniform);

    /**
     *  @brief
     *      Gets the location of an attribute.
     *
     *  @param [in] attribute
     *      The name of the attribute.
     *
     *  @return
     *      The location of the attribute.
     */
    int GetAttribute(std::string attribute);

    /**
     *  @brief
     *      Sets the ModelViewProjection matrix.
     *
     *  @param [in] modelViewProjectionMatrix
     *      The ModelViewProjection matrix.
     *
     *  All shaders must define a uniform mat4 named "modelViewProjectionMatrix".
     */
    void SetModelViewProjectionMatrix(Mat4x4f modelViewProjectionMatrix);

    /**
     *  @brief
     *      Sets the ambient color.
     *
     *  @param [in] ambientColor
     *      The ambient color.
     *
     *  All shaders must define a uniform vec3 named "ambientColor".
     */
    void SetAmbientColor(Vec3f ambientColor);

    /**
     *  @brief
     *      Sets the diffuse color.
     *
     *  @param [in] diffuseColor
     *      The diffuse color.
     *
     *  All shaders must define a uniform vec3 named "diffuseColor".
     */
    void SetDiffuseColor(Vec3f diffuseColor);

    /**
     *  @brief
     *      Sets the specular color.
     *
     *  @param [in] specularColor
     *      The specular color.
     *
     *  All shaders must define a uniform vec3 named "specularColor".
     */
    void SetSpecularColor(Vec3f specularColor);

    /**
     *  @brief
     *      Sets the specular exponent.
     *
     *  @param [in] specularExponent
     *      The specular exponent.
     *
     *  All shaders must define a uniform float named "specularExponent".
     */
    void SetSpecularExponent(Vec3f specularExponent);

    /**
     *  @brief
     *      Sets the alpha.
     *
     *  @param [in] alpha
     *      The alpha.
     *
     *  All shaders must define a uniform float named "alpha".
     */
    void SetAlpha(float alpha);

protected:
    /**
     *  @brief
     *      Registers a uniform.
     *
     *  @param [in] uniform
     *      The name of the uniform to register.
     */
    void RegisterUniform(std::string uniform);

    /**
     *  @brief
     *      Registers an attribute.
     *
     *  @param [in] attribute
     *      The name of the attribute to register.
     */
    void RegisterAttribute(std::string attribute);

    /**
     *  @brief
     *      Sets the value of a one dimensional float uniform.
     *
     *  @param [in] uniform
     *      The name of the uniform.
     *
     *  @param [in] value
     *      The value to set.
     */
    void SetUniform(std::string uniform, float value);

    /**
     *  @brief
     *      Sets the value of a two dimensional float uniform.
     *
     *  @param [in] uniform
     *      The name of the uniform.
     *
     *  @param [in] value
     *      The value to set.
     */
    void SetUniform(std::string uniform, const Vec2f& value);

    /**
     *  @brief
     *      Sets the value of a three dimensional float uniform.
     *
     *  @param [in] uniform
     *      The name of the uniform.
     *
     *  @param [in] value
     *      The value to set.
     */
    void SetUniform(std::string uniform, const Vec3f& value);

    /**
     *  @brief
     *      Sets the value of a four dimensional float uniform.
     *
     *  @param [in] uniform
     *      The name of the uniform.
     *
     *  @param [in] value
     *      The value to set.
     */
    void SetUniform(std::string uniform, const Vec4f& value);

    /**
     *  @brief
     *      Sets the value of a one dimensional int uniform.
     *
     *  @param [in] uniform
     *      The name of the uniform.
     *
     *  @param [in] value
     *      The value to set.
     */
    void SetUniform(std::string uniform, int value);

    /**
     *  @brief
     *      Sets the value of a two dimensional int uniform.
     *
     *  @param [in] uniform
     *      The name of the uniform.
     *
     *  @param [in] value
     *      The value to set.
     */
    void SetUniform(std::string uniform, const Vec2i& value);

    /**
     *  @brief
     *      Sets the value of a three dimensional int uniform.
     *
     *  @param [in] uniform
     *      The name of the uniform.
     *
     *  @param [in] value
     *      The value to set.
     */
    void SetUniform(std::string uniform, const Vec3i& value);

    /**
     *  @brief
     *      Sets the value of a four dimensional int uniform.
     *
     *  @param [in] uniform
     *      The name of the uniform.
     *
     *  @param [in] value
     *      The value to set.
     */
    void SetUniform(std::string uniform, const Vec4i& value);

    /**
     *  @brief
     *      Sets the value of a 3x3 float matrix uniform.
     *
     *  @param [in] uniform
     *      The name of the uniform.
     *
     *  @param [in] value
     *      The value to set.
     */
    void SetUniform(std::string uniform, const Mat3x3f& value);

    /**
     *  @brief
     *      Sets the value of a 4x4 float matrix uniform.
     *
     *  @param [in] uniform
     *      The name of the uniform.
     *
     *  @param [in] value
     *      The value to set.
     */
    void SetUniform(std::string uniform, const Mat4x4f& value);

private:
    /**
     *  @brief
     *      A handle to the shader program.
     */
    unsigned int program;

    /**
     *  @brief
     *      A handle to the vertex shader.
     */
    unsigned int vertexShader;

    /**
     *  @brief
     *      A handle to the fragment shader.
     */
    unsigned int fragmentShader;

    /**
     *  @brief
     *      A map of all uniforms to their locations.
     */
    std::map<std::string, int> uniforms;

    /**
     *  @brief
     *      A map of all attributes to their locations.
     */
    std::map<std::string, int> attributes;

    /**
     *  @brief
     *      Loads and compiles a shader.
     *
     *  @param [in] filename
     *      The filename of the shader to load.
     *
     *  @return
     *      A handle to the loaded shader.
     *
     *  @retval -1
     *      The shader failed to load or compile.
     */
    unsigned int LoadShader(std::string filename);
};

#endif /* Shader_hpp */
