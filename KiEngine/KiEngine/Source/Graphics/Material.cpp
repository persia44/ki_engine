/**
 *  @file
 *      Material.cpp
 *
 *  @brief
 *      Contains a Material class.
 */

#include "Material.hpp"

#include <string>
#include <sstream>

#include "Resource.hpp"
#include "File.hpp"

Material::Material(std::string filename, Shader& shader, Texture& texture) :
    shader(shader),
    texture(texture)
{
    // Load the material data
    File materialData = Resource::Load(Resource::Material, filename);

    // Parse the data line by line
    for (std::string line; materialData.GetLine(line);)
    {
        std::istringstream data(line);
        std::string dataType;
        data >> dataType;

        if (dataType == "newmtl") // A new material
        {
            // We are assuming each MTL file only defines a single material.
            // Therefore, do nothing here.
        }
        else if (dataType == "Ka") // Set the ambient color
        {
            float r, g, b;
            data >> r >> g >> b;
            ambientColor = Vec3f(r, g, b);
        }
        else if (dataType == "Kd") // Set the diffuse color
        {
            float r, g, b;
            data >> r >> g >> b;
            diffuseColor = Vec3f(r, g, b);
        }
        else if (dataType == "Ks") // Set the specular color
        {
            float r, g, b;
            data >> r >> g >> b;
            specularColor = Vec3f(r, g, b);
        }
        else if (dataType == "Ns") // Set the specular exponent
        {
            data >> specularExponent;
        }
        else if (dataType == "d") // Set the alpha
        {
            data >> alpha;
        }
    }
}

Material::Material(Vec3f ambientColor, float alpha, Shader& shader, Texture& texture) :
    ambientColor(ambientColor),
    alpha(alpha),
    shader(shader),
    texture(texture)
{
}

unsigned int Material::GetVertexPositionAttribute()
{
    return shader.GetAttribute("vertexPosition");
}

unsigned int Material::GetVertexTextureCoordinatesAttribute()
{
    return shader.GetAttribute("vertexTextureCoordinates");
}

void Material::SetModelViewProjectionMatrix(Mat4x4f modelViewProjectionMatrix)
{
    shader.SetModelViewProjectionMatrix(modelViewProjectionMatrix);
}

void Material::Use()
{
    shader.Use();
    texture.Use();

    // Set the colors
    shader.SetAmbientColor(ambientColor);
    shader.SetDiffuseColor(diffuseColor);
    shader.SetSpecularColor(specularColor);
    shader.SetSpecularExponent(specularExponent);
    shader.SetAlpha(alpha);
}
