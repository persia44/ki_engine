/**
 *  @file
 *      Texture.hpp
 *
 *  @brief
 *      Contains a Texture class.
 */

#ifndef Texture_hpp
#define Texture_hpp

#include <iosfwd>

/**
 *  @brief
 *      A Texture is added to a Material to modify its appearance.
 */
class Texture
{
public:
    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] filename
     *      The file containing the texture information.
     */
    Texture(std::string filename);

    /**
     *  @brief
     *      Destructor.
     */
    ~Texture();

    /**
     *  @brief
     *      Use the Texture.
     */
    void Use();

private:
    /**
     *  @brief
     *      A handle to the texture.
     */
    unsigned int texture;
};

#endif /* Texture_hpp */
