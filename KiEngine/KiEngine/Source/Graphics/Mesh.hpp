/**
 *  @file
 *      Mesh.hpp
 *
 *  @brief
 *      Contains a Mesh class for drawing a mesh.
 */

#ifndef Mesh_hpp
#define Mesh_hpp

#include <iosfwd>
#include <vector>

#include "Vertex.hpp"
#include "Vector.hpp"
#include "Matrix.hpp"
#include "Material.hpp"

/**
 *  @brief
 *      A Mesh consists of vertices which are joined
 *      to create an object which can be rendered.
 */
class Mesh
{
public:
    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] filename
     *      An OBJ file containing geometry information for the Mesh.
     *
     *  @param [in] material
     *      The Material of the Mesh.
     *
     *  @param [in] position
     *      The position of the Mesh. Defaults to (0, 0, 0).
     */
    Mesh(std::string filename, Material& material, Vec3f position = Vec3f());

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] vertices
     *      The vertices to add to the Mesh.
     *
     *  @param [in] indices
     *      A list of sets of three vertex indices
     *      which specify triangles for the Mesh.
     *
     *  @param [in] material
     *      The Material of the Mesh.
     *
     *  @param [in] position
     *      The position of the Mesh. Defaults to (0, 0, 0).
     */
    Mesh(std::vector<Vertex> vertices, std::vector<int> indices, Material& material, Vec3f position = Vec3f());

    /**
     *  @brief
     *      Destructor.
     */
    ~Mesh();

    /**
     *  @brief
     *      Sets the ViewProjection matrix.
     *
     *  @param [in] viewProjectionMatrix
     *      The ViewProjection matrix.
     */
    void SetViewProjectionMatrix(Mat4x4f viewProjectionMatrix);

    /**
     *  @brief
     *      Renders the Mesh.
     */
    void Render();

    /**
     *  @brief
     *      Sets the position of the Mesh.
     *
     *  @param [in] position
     *      The position to set.
     */
    void SetPosition(Vec3f position);

    /**
     *  @brief
     *      Moves the Mesh.
     *
     *  @param [in] displacement
     *      The displacement (in world coordinates) to move.
     */
    void Move(Vec3f displacement);

private:
    /**
     *  @brief
     *      The vertices of the Mesh.
     */
    std::vector<Vertex> vertices;
    /**
     *  @brief
     *      The vertex indices of each triangle.
     */
    std::vector<int> indices;

    /**
     *  @brief
     *      A handle to a vertex array object.
     */
    unsigned int vertexArrayObject;

    /**
     *  @brief
     *      A handle to a vertex buffer object.
     */
    unsigned int vertexBufferObject;

    /**
     *  @brief
     *      A handle to an index buffer object.
     */
    unsigned int indexBufferObject;

    /**
     *  @brief
     *      The Material of the Mesh.
     */
    Material material;

    /**
     *  @brief
     *      The position of the centre of the Mesh.
     */
    Vec3f position;

    /**
     *  @brief
     *      The View Projection Matrix.
     */
    Mat4x4f viewProjectionMatrix;

    /**
     *  @brief
     *      Initializes the Mesh.
     */
    void Initialize();
};

#endif /* Mesh_hpp */
