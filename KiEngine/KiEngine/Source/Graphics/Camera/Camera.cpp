/**
 *  @file
 *      Camera.cpp
 *
 *  @brief
 *      Contains a Camera class.
 */

#include "Camera.hpp"

#include "Vector.hpp"
#include "Matrix.hpp"
#include "Quaternion.hpp"

#include "Game.hpp"
#include "Window.hpp"
#include "Input.hpp"
#include "Time.hpp"
#include "Math.hpp"

Camera::Camera(Vec3f position, Vec3f forward, Vec3f up,
               float fieldOfView, float zNear, float zFar) :
    position(position),
    forward(Normalize(forward)),
    up(Normalize(up)),
    fieldOfView(fieldOfView),
    zNear(zNear),
    zFar(zFar)
{
    right = Cross(this->up, this->forward);
}

Mat4x4f Camera::GetViewMatrix()
{
    // Move the Camera to the world's origin.
    Mat4x4f viewTranslation = Mat4x4f::Identity();
    viewTranslation[0][3] = -position.x;
    viewTranslation[1][3] = -position.y;
    viewTranslation[2][3] = -position.z;

    float v = sqrtf(powf(forward.x, 2) + powf(forward.z, 2));

    // Align the Camera's forward direction to the y/z plane
    Mat4x4f viewYRotation(
    {
        forward.z / v,  0,  -forward.x / v, 0,
        0,              1,  0,              0,
        forward.x / v,  0,  forward.z / v,  0,
        0,              0,  0,              1
    });

    // Align the Camera's forward direction to the z-axis
    Mat4x4f viewXRotation(
    {
        1,  0,          0,          0,
        0,  v,          -forward.y, 0,
        0,  forward.y,  v,          0,
        0,  0,          0,          1
    });

    // Align the Camera's up direction to the y-axis
    Vec3f newUp = Normalize((viewXRotation * viewYRotation * Vec4f(up, 1)).xyz);
    Mat4x4f viewZRotation(
    {
        newUp.y,    -newUp.x,   0,  0,
        newUp.x,    newUp.y,    0,  0,
        0,          0,          1,  0,
        0,          0,          0,  1
    });

    // Combine all transformations
    return viewZRotation * viewXRotation * viewYRotation * viewTranslation;
}

Mat4x4f Camera::GetProjectionMatrix()
{
    float aspectRatio = Game::Get().GetWindow().GetAspectRatio();
    float f = 1 / tanf(Math::ToRadians(fieldOfView / 2));
    float zRange = zNear - zFar;
    return Mat4x4f(
    {
        f / aspectRatio,    0,  0,                          0,
        0,                  f,  0,                          0,
        0,                  0,  (-zNear - zFar) / zRange,   2 * zFar * zNear / zRange,
        0,                  0,  1,                          0
    });
}

Mat4x4f Camera::GetViewProjectionMatrix()
{
    return GetProjectionMatrix() * GetViewMatrix();
}

void Camera::Update()
{
}

void Camera::Move(Vec3f displacement)
{
    position += displacement;
}

void Camera::Yaw(float angle)
{
    right   = Normalize(Math::Rotate(right,     up, angle));
    forward = Normalize(Math::Rotate(forward,   up, angle));
}

void Camera::Pitch(float angle)
{
    up      = Normalize(Math::Rotate(up,        right, angle));
    forward = Normalize(Math::Rotate(forward,   right, angle));
}

void Camera::Roll(float angle)
{
    right   = Normalize(Math::Rotate(right,     forward, angle));
    up      = Normalize(Math::Rotate(up,        forward, angle));
}
