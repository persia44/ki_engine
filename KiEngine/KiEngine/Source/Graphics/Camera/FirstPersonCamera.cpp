/**
 *  @file
 *      FirstPersonCamera.cpp
 *
 *  @brief
 *      Contains a FirstPersonCamera class.
 */

#include "FirstPersonCamera.hpp"

#include "Input.hpp"
#include "Time.hpp"
#include "Math.hpp"

FirstPersonCamera::FirstPersonCamera(Vec3f position, Vec3f forward, Vec3f up,
                                     float fieldOfView, float zNear, float zFar) :
    Camera(position, forward, up, fieldOfView, zNear, zFar)
{
}

Mat4x4f FirstPersonCamera::GetViewMatrix()
{
    // Move the Camera to the world's origin.
    Mat4x4f viewTranslation = Mat4x4f::Identity();
    viewTranslation[0][3] = -position.x;
    viewTranslation[1][3] = -position.y;
    viewTranslation[2][3] = -position.z;

    float v = sqrtf(powf(forward.x, 2) + powf(forward.z, 2));

    // Align the Camera's forward direction to the y/z plane
    Mat4x4f viewYRotation(
    {
        forward.z / v,  0,  -forward.x / v, 0,
        0,              1,  0,              0,
        forward.x / v,  0,  forward.z / v,  0,
        0,              0,  0,              1
    });

    // Align the Camera's forward direction to the z-axis
    Mat4x4f viewXRotation(
    {
        1,  0,          0,          0,
        0,  v,          -forward.y, 0,
        0,  forward.y,  v,          0,
        0,  0,          0,          1
    });

    // Combine all transformations
    return viewXRotation * viewYRotation * viewTranslation;
}

void FirstPersonCamera::Update()
{
    float movementAmount = movementSensitivity * Time::GetDeltaTime();

    if (Input::IsPressed(Key::W))
    {
        MoveForward(movementAmount);
    }
    if (Input::IsPressed(Key::S))
    {
        MoveBackward(movementAmount);
    }
    if (Input::IsPressed(Key::A))
    {
        MoveLeft(movementAmount);
    }
    if (Input::IsPressed(Key::D))
    {
        MoveRight(movementAmount);
    }
    if (Input::IsPressed(Key::Q))
    {
        MoveDown(movementAmount);
    }
    if (Input::IsPressed(Key::E))
    {
        MoveUp(movementAmount);
    }

    LookDown(lookSensitivity * Time::GetDeltaTime() * Input::GetMouseDelta().y);
    LookRight(lookSensitivity * Time::GetDeltaTime() * Input::GetMouseDelta().x);
}

void FirstPersonCamera::MoveForward(float distance)
{
    Camera::Move(Vec3f(forward.x, 0, forward.z) * distance);
}

void FirstPersonCamera::MoveBackward(float distance)
{
    Camera::Move(Vec3f(-forward.x, 0, -forward.z) * distance);
}

void FirstPersonCamera::MoveLeft(float distance)
{
    Camera::Move(-right * distance);
}

void FirstPersonCamera::MoveRight(float distance)
{
    Camera::Move(right * distance);
}

void FirstPersonCamera::MoveUp(float distance)
{
    Camera::Move(Vec3f(0, 1, 0) * distance);
}

void FirstPersonCamera::MoveDown(float distance)
{
    Camera::Move(Vec3f(0, -1, 0) * distance);
}

void FirstPersonCamera::LookLeft(float angle)
{
    right   = Normalize(Math::Yaw(right,    -angle));
    up      = Normalize(Math::Yaw(up,       -angle));
    forward = Normalize(Math::Yaw(forward,  -angle));
}

void FirstPersonCamera::LookRight(float angle)
{
    right   = Normalize(Math::Yaw(right,    angle));
    up      = Normalize(Math::Yaw(up,       angle));
    forward = Normalize(Math::Yaw(forward,  angle));
}

void FirstPersonCamera::LookUp(float angle)
{
    up      = Normalize(Math::Rotate(up,        right, -angle));
    forward = Normalize(Math::Rotate(forward,   right, -angle));
}

void FirstPersonCamera::LookDown(float angle)
{
    up      = Normalize(Math::Rotate(up,        right, angle));
    forward = Normalize(Math::Rotate(forward,   right, angle));
}
