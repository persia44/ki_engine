/**
 *  @file
 *      Camera.hpp
 *
 *  @brief
 *      Contains a Camera class.
 */

#ifndef Camera_hpp
#define Camera_hpp

#include "Vector.hpp"
#include "Matrix.hpp"

/**
 *  @brief
 *      A Camera is used for specifying a way to view the world.
 */
class Camera
{
public:
    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] position
     *      The position of the Camera in world coordinates.
     *
     *  @param [in] forward
     *      A direction Vector, in world coordinates,
     *      pointing forward from the Camera.
     *
     *  @param [in] position
     *      A direction Vector, in world coordinates,
     *      pointing upward from the Camera.
     *
     *  @param [in] fieldOfView
     *      The viewable angle (in degrees).
     *
     *  @param [in] zNear
     *      The near clipping plane.
     *
     *  @param [in] zFar
     *      The far clipping plane.
     */
    Camera(Vec3f position       = Vec3f(0, 0, 0),
           Vec3f forward        = Vec3f(0, 0, 1),
           Vec3f up             = Vec3f(0, 1, 0),
           float fieldOfView    = 70,
           float zNear          = 0.1,
           float zFar           = 1000);

    /**
     *  @brief
     *      Calculates the View matrix.
     *
     *  @return
     *      The View matrix.
     */
    virtual Mat4x4f GetViewMatrix();

    /**
     *  @brief
     *      Calculates the Projection matrix.
     *
     *  @return
     *      The Projection matrix.
     */
    virtual Mat4x4f GetProjectionMatrix();

    /**
     *  @brief
     *      Calculates the ViewProjection matrix.
     *
     *  @return
     *      The ViewProjection matrix.
     */
    Mat4x4f GetViewProjectionMatrix();

    /**
     *  @brief
     *      Updates the Camera.
     */
    virtual void Update();

    /**
     *  @brief
     *      Moves the Camera.
     *
     *  @param [in] displacement
     *      The displacement (in world coordinates) to move.
     */
    virtual void Move(Vec3f displacement);

    /**
     *  @brief
     *      Yaws the Camera.
     *
     *  @param [in] angle
     *      The angle (in degrees) to yaw.
     *
     *  A yaw is a rotation about the y-axis.
     *  A negative angle yaws to the left; a positive angle yaws to the right.
     */
    virtual void Yaw(float angle);

    /**
     *  @brief
     *      Pitches the Camera.
     *
     *  @param [in] angle
     *      The angle (in degrees) to pitch.
     *
     *  A pitch is a rotation about the x-axis.
     *  A negative angle pitches up; a positive angle pitches down.
     */
    virtual void Pitch(float angle);

    /**
     *  @brief
     *      Rolls the Camera.
     *
     *  @param [in] angle
     *      The angle (in degrees) to roll.
     *
     *  A roll is a rotation about the z-axis.
     *  A positive angle rolls to the left; a negative angle rolls to the right.
     */
    virtual void Roll(float angle);

protected:
    /**
     *  @brief
     *      The position, in world coordinates, of the Camera.
     */
    Vec3f position;

    /**
     *  @brief
     *      The direction of the axes, in world coordinates, of the Camera.
     */
    /// @{
    Vec3f forward, up, right;
    /// @}

    /**
     *  @brief
     *      The viewable angle (in degrees).
     *
     *  This angle defines a cone which expands outwards from the Camera.
     *  Anything outside of this cone is not viewable.
     */
    float fieldOfView;

    /**
     *  @brief
     *      The near and far clipping planes.
     *
     *  Anything closer than \a zNear or further than
     *  \a zFar (in the z-axis) is not viewable.
     */
    float zNear, zFar;

    /**
     *  @brief
     *      The Camera's rotation sensitivity.
     */
    float lookSensitivity = 0.01;

    /**
     *  @brief
     *      The Camera's movement sensitivity.
     */
    float movementSensitivity = 0.01;
};

#endif /* Camera_hpp */
