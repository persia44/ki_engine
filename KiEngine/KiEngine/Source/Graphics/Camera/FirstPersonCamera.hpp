/**
 *  @file
 *      FirstPersonCamera.hpp
 *
 *  @brief
 *      Contains a FirstPersonCamera class.
 */

#ifndef FirstPersonCamera_hpp
#define FirstPersonCamera_hpp

#include "Camera.hpp"

class FirstPersonCamera : public Camera
{
public:
    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] position
     *      The position of the FirstPersonCamera in world coordinates.
     *
     *  @param [in] forward
     *      A direction Vector, in world coordinates,
     *      pointing forward from the FirstPersonCamera.
     *
     *  @param [in] position
     *      A direction Vector, in world coordinates,
     *      pointing upward from the FirstPersonCamera.
     *
     *  @param [in] fieldOfView
     *      The viewable angle (in degrees).
     *
     *  @param [in] zNear
     *      The near clipping plane.
     *
     *  @param [in] zFar
     *      The far clipping plane.
     */
    FirstPersonCamera(Vec3f position       = Vec3f(0, 0, 0),
                      Vec3f forward        = Vec3f(0, 0, 1),
                      Vec3f up             = Vec3f(0, 1, 0),
                      float fieldOfView    = 70,
                      float zNear          = 0.1,
                      float zFar           = 1000);

    /**
     *  @brief
     *      Calculates the View matrix.
     *
     *  @return
     *      The View matrix.
     */
    Mat4x4f GetViewMatrix();

    /**
     *  @brief
     *      Updates the FirstPersonCamera.
     */
    void Update();

    /**
     *  @brief
     *      Moves the FirstPersonCamera forward.
     *
     *  @param [in] distance
     *      The distance to move.
     */
    void MoveForward(float distance);

    /**
     *  @brief
     *      Moves the FirstPersonCamera backward.
     *
     *  @param [in] distance
     *      The distance to move.
     */
    void MoveBackward(float distance);

    /**
     *  @brief
     *      Moves the FirstPersonCamera left.
     *
     *  @param [in] distance
     *      The distance to move.
     */
    void MoveLeft(float distance);

    /**
     *  @brief
     *      Moves the FirstPersonCamera right.
     *
     *  @param [in] distance
     *      The distance to move.
     */
    void MoveRight(float distance);

    /**
     *  @brief
     *      Moves the FirstPersonCamera up.
     *
     *  @param [in] distance
     *      The distance to move.
     */
    void MoveUp(float distance);

    /**
     *  @brief
     *      Moves the FirstPersonCamera down.
     *
     *  @param [in] distance
     *      The distance to move.
     */
    void MoveDown(float distance);

    /**
     *  @brief
     *      Rotates the FirstPersonCamera to the left.
     *
     *  @param [in] angle
     *      The angle (in degrees) to rotate.
     */
    void LookLeft(float angle);

    /**
     *  @brief
     *      Rotates the FirstPersonCamera to the right.
     *
     *  @param [in] angle
     *      The angle (in degrees) to rotate.
     */
    void LookRight(float angle);

    /**
     *  @brief
     *      Rotates the FirstPersonCamera to upwards.
     *
     *  @param [in] angle
     *      The angle (in degrees) to rotate.
     */
    void LookUp(float angle);

    /**
     *  @brief
     *      Rotates the FirstPersonCamera to downwards.
     *
     *  @param [in] angle
     *      The angle (in degrees) to rotate.
     */
    void LookDown(float angle);
};

#endif /* FirstPersonCamera_hpp */
