/**
 *  @file
 *      Render.cpp
 *
 *  @brief
 *      Contains functions related to rendering graphics.
 */

#include "Render.hpp"

#include <GL/glew.h>

namespace Render
{

    /**
     *  Initialize GLEW and set OpenGL settings
     */
    void Initialize()
    {
        // Initialize GLEW
        glewExperimental = true;
        glewInit();

        // Set the clear color to black
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        // Enable face culling
        // Faces are front facing if their vertices are defined in a clockwise order
        glFrontFace(GL_CW);
        glCullFace(GL_BACK);
        glEnable(GL_CULL_FACE);

        // Enable depth testing
        glEnable(GL_DEPTH_TEST);

        glEnable(GL_FRAMEBUFFER_SRGB);
    }

    void Clear()
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

}
