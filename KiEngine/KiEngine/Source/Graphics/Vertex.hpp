/**
 *  @file
 *      Vertex.hpp
 *
 *  @brief
 *      Contains a Vertex type for use in graphics.
 */

#ifndef Vertex_hpp
#define Vertex_hpp

#include "Vector.hpp"

/**
 *  @brief
 *      A Vertex class.
 */
class Vertex
{
public:
    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] x
     *      The x position of the Vertex.
     *
     *  @param [in] y
     *      The y position of the Vertex.
     *
     *  @param [in] z
     *      The z position of the Vertex.
     */
    Vertex(float x, float y, float z);

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] position
     *      The position of the Vertex.
     */
    Vertex(Vec3f position);

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] x
     *      The x position of the Vertex.
     *
     *  @param [in] y
     *      The y position of the Vertex.
     *
     *  @param [in] z
     *      The z position of the Vertex.
     *
     *  @param [in] u
     *      The u texture coordinate of the Vertex.
     *
     *  @param [in] v
     *      The v texture coordinate of the Vertex.
     */
    Vertex(float x, float y, float z, float u, float v);

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] position
     *      The position of the Vertex.
     *
     *  @param [in] textureCoordinates
     *      The texture coordinates of the Vertex.
     */
    Vertex(Vec3f position, Vec2f textureCoordinates);

private:
    /**
     *  @brief
     *      The Vertex position.
     */
    Vec3f position;

    /**
     *  @brief
     *      The Vertex texture coordinates.
     */
    Vec2f textureCoordinates;
};

#endif /* Vertex_hpp */
