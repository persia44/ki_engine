/**
 *  @file
 *      Material.hpp
 *
 *  @brief
 *      Contains a Material class.
 */

#ifndef Material_hpp
#define Material_hpp

#include <iosfwd>

#include "Vector.hpp"
#include "Matrix.hpp"
#include "Texture.hpp"
#include "Shader.hpp"

/**
 *  @brief
 *      A Material is added to a Mesh to define its appearance.
 */
class Material
{
public:
    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] filename
     *      An MTL file containing information about the Material.
     *
     *  @param [in] shader
     *      The Shader to apply to this Material.
     *
     *  @param [in] texture
     *      The Texture to apply to this Material. This parameter is optional.
     *
     *  We are assuming the MTL file only defines a single material.
     */
    Material(std::string filename, Shader& shader, Texture& texture);

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] ambientColor
     *      The ambient color.
     *
     *  @param [in] alpha
     *      The alpha.
     *
     *  @param [in] shader
     *      The Shader to apply to this Material.
     *
     *  @param [in] texture
     *      The Texture to apply to this Material. This parameter is optional.
     */
    Material(Vec3f ambientColor, float alpha, Shader& shader, Texture& texture);

    /**
     *  @brief
     *      Gets the location of the vertexPosition Shader attribute.
     *
     *  @return
     *      The vertexPosition attribute location.
     */
    unsigned int GetVertexPositionAttribute();

    /**
     *  @brief
     *      Gets the location of the vertexTextureCoordinates Shader attribute.
     *
     *  @return
     *      The vertexTextureCoordinates attribute location.
     */
    unsigned int GetVertexTextureCoordinatesAttribute();

    /**
     *  @brief
     *      Sets the ModelViewProjection matrix.
     *
     *  @param [in] modelViewProjectionMatrix
     *      The ModelViewProjection matrix.
     */
    void SetModelViewProjectionMatrix(Mat4x4f modelViewProjectionMatrix);

    /**
     *  @brief
     *      Use the Material.
     */
    void Use();

private:
    /**
     *  @brief
     *      The ambient color.
     */
    Vec3f ambientColor;

    /**
     *  @brief
     *      The diffuse color.
     */
    Vec3f diffuseColor;

    /**
     *  @brief
     *      The specular color.
     */
    Vec3f specularColor;

    /**
     *  @brief
     *      The specular exponent.
     */
    float specularExponent;

    /**
     *  @brief
     *      The alpha (transparency).
     */
    float alpha;

    /**
     *  @brief
     *      The Material Shader.
     */
    Shader shader;

    /**
     *  @brief
     *      The Material Texture.
     */
    Texture texture;
};

#endif /* Material_hpp */
