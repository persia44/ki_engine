/**
 *  @file
 *      Vertex.cpp
 *
 *  @brief
 *      Contains a Vertex type for use in graphics.
 */

#include "Vertex.hpp"

Vertex::Vertex(float x, float y, float z) :
    Vertex(Vec3f(x, y, z))
{
}

Vertex::Vertex(Vec3f position) :
    position(position)
{
}

Vertex::Vertex(float x, float y, float z, float u, float v) :
    Vertex(Vec3f(x, y, z), Vec2f(u, v))
{
}

Vertex::Vertex(Vec3f position, Vec2f textureCoordinates) :
    position(position),
    textureCoordinates(textureCoordinates)
{
}
