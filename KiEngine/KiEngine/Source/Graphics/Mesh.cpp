/**
 *  @file
 *      Mesh.cpp
 *
 *  @brief
 *      Contains a Mesh class for drawing a mesh.
 */

#include "Mesh.hpp"

#include <string>
#include <sstream>
#include <vector>

#include "Resource.hpp"
#include "Vertex.hpp"

#include <Gl/glew.h>

Mesh::Mesh(std::string filename, Material& material, Vec3f position) :
    material(material),
    position(position)
{
    // Load the mesh data
    File meshData = Resource::Load(Resource::Mesh, filename);

    // Parse the data line by line
    for (std::string line; meshData.GetLine(line);)
    {
        std::istringstream data(line);
        std::string dataType;
        data >> dataType;

        if (dataType == "mtllib") // Load a material
        {
            /// @todo Fix material loading

//            std::string materialFilename;
//            data >> materialFilename;
//            material = Material(materialFilename);
        }
        else if (dataType == "usemtl") // Use a material
        {
            // We are assuming each MTL file only defines a single material.
            // Therefore, do nothing here.
        }
        else if (dataType == "v") // Add the vertex data
        {
            float x, y, z;
            data >> x >> y >> z;
            /// @todo Get texture coordinates
            vertices.push_back(Vertex(x, y, z));
        }
        else if (dataType == "f") // Add the face data
        {
            int a, b, c;
            data >> a >> b >> c;
            // Indices in OBJ format start from 1, whereas we start from 0
            indices.push_back(a - 1);
            indices.push_back(b - 1);
            indices.push_back(c - 1);
        }
    }

    Initialize();
}

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<int> indices, Material& material, Vec3f position) :
    vertices(vertices),
    indices(indices),
    material(material),
    position(position)
{
    Initialize();
}

Mesh::~Mesh()
{
    glDeleteBuffers(1, &vertexBufferObject);
    glDeleteVertexArrays(1, &vertexArrayObject);
}

void Mesh::SetViewProjectionMatrix(Mat4x4f viewProjectionMatrix)
{
    this->viewProjectionMatrix = viewProjectionMatrix;
}

void Mesh::Render()
{
    glBindVertexArray(vertexArrayObject);

    Mat4x4f modelMatrix(
    {
        1, 0, 0, position.x,
        0, 1, 0, position.y,
        0, 0, 1, position.z,
        0, 0, 0, 1
    });

    material.SetModelViewProjectionMatrix(viewProjectionMatrix * modelMatrix);
    material.Use();

    // Draw the mesh
    glDrawElements(GL_TRIANGLES, static_cast<int>(indices.size()), GL_UNSIGNED_INT, nullptr);

    glBindVertexArray(0);
}

void Mesh::SetPosition(Vec3f position)
{
    this->position = position;
}

void Mesh::Move(Vec3f displacement)
{
    position += displacement;
}

void Mesh::Initialize()
{
    // Set up the vertex array object
    glGenVertexArrays(1, &vertexArrayObject);
    glBindVertexArray(vertexArrayObject);

    // Set up the vertex buffer object
    glGenBuffers(1, &vertexBufferObject);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);

    // Set up the index buffer object
    glGenBuffers(1, &indexBufferObject);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(int), indices.data(), GL_STATIC_DRAW);

    // Set up the vertexPosition attribute array
    GLuint vertexPosition = material.GetVertexPositionAttribute();
    glEnableVertexAttribArray(vertexPosition);
    glVertexAttribPointer(vertexPosition, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);

    // Set up the vertexTextureCoordinates attribute array
    GLuint vertexTextureCoordinates = material.GetVertexTextureCoordinatesAttribute();
    glEnableVertexAttribArray(vertexTextureCoordinates);
    glVertexAttribPointer(vertexTextureCoordinates, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) sizeof(Vec3f));

    // Unbind the VAO
    glBindVertexArray(0);
}
