/**
 *  @file
 *      Quaternion.hpp
 *
 *  @brief
 *      Contains a Quaternion type along with related functions.
 */

#ifndef Quaternion_hpp
#define Quaternion_hpp

#include <iosfwd>

#include "Vector.hpp"
#include "Matrix.hpp"

/**
 *  @brief
 *      A mathematical quaternion.
 */
struct Quaternion
{
    /**
     *  @name
     *      The Quaternion values.
     */
    /// @{
    float w, x, y, z;
    /// @}

    /**
     *  @brief
     *      Constructor.
     *
     *  Constructs a Quaternion with the value (1, 0, 0, 0).
     */
    Quaternion();

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] w
     *      Value to set the \a w component to.
     *
     *  @param [in] x
     *      Value to set the \a x component to.
     *
     *  @param [in] y
     *      Value to set the \a y component to.
     *
     *  @param [in] z
     *      Value to set the \a z component to.
     */
    Quaternion(float w, float x, float y, float z);
};

/**
 *  @brief
 *      Adds two Quaternions and assigns the result to the left hand side Quaternion.
 *
 *  @param [in, out] lhs
 *      The first Quaternion to add and have the result assigned to.
 *
 *  @param [in] rhs
 *      The second Quaternion to add.
 *
 *  @return
 *      \a lhs after the add operation.
 */
Quaternion& operator+=(Quaternion& lhs, const Quaternion& rhs);

/**
 *  @brief
 *      Subtracts two Quaternions and assigns the result to the left hand side Quaternion.
 *
 *  @param [in, out] lhs
 *      The Quaternion to subtract from and have the result assigned to.
 *
 *  @param [in] rhs
 *      The Quaternion to subtract by.
 *
 *  @return
 *      \a lhs after the subtract operation.
 */
Quaternion& operator-=(Quaternion& lhs, const Quaternion& rhs);

/**
 *  @brief
 *      Multiplies a Quaternion by a scalar value and assigns the result to the Quaternion.
 *
 *  @param [in, out] quaternion
 *      The Quaternion to multiply and have the result assigned to.
 *
 *  @param [in] scalar
 *      The scalar value to multiply by.
 *
 *  @return
 *      \a quaternion after the multiply operation.
 */
Quaternion& operator*=(Quaternion& quaternion, float scalar);

/**
 *  @brief
 *      Multiplies two Quaternions and assigns the result to the left hand side Quaternion.
 *
 *  @param [in, out] lhs
 *      The first Quaternion to multiply and have the result assigned to.
 *
 *  @param [in] rhs
 *      The second Quaternion to multiply.
 *
 *  @return
 *      \a lhs after the multiply operation.
 */
Quaternion& operator*=(Quaternion& lhs, const Quaternion& rhs);

/**
 *  @brief
 *      Multiplies a Quaternion by a Vector and assigns the result to the Quaternion.
 *
 *  @param [in, out] quaternion
 *      The Quaternion to multiply and have the result assigned to.
 *
 *  @param [in] vector
 *      The vector to multiply by.
 *
 *  @return
 *      \a quaternion after the multiply operation.
 */
Quaternion& operator*=(Quaternion& quaternion, const Vec3f& vector);

/**
 *  @brief
 *      Divides a Quaternion by a scalar value and assigns the result to the Quaternion.
 *
 *  @param [in, out] quaternion
 *      The Quaternion to divide and have the result assigned to.
 *
 *  @param [in] scalar
 *      The scalar value to divide by.
 *
 *  @return
 *      \a quaternion after the divide operation.
 */
Quaternion& operator/=(Quaternion& quaternion, float scalar);

/**
 *  @brief
 *      Adds two Quaternions.
 *
 *  @param [in] lhs
 *      The first Quaternion to add.
 *
 *  @param [in] rhs
 *      The second Quaternion to add.
 *
 *  @return
 *      The sum of \a lhs and \a rhs.
 */
const Quaternion operator+(Quaternion lhs, const Quaternion& rhs);

/**
 *  @brief
 *      Subtracts two Quaternions.
 *
 *  @param [in] lhs
 *      The Quaternion to subtract from.
 *
 *  @param [in] rhs
 *      The Quaternion to subtract by.
 *
 *  @return
 *      \a lhs subtracted by \a rhs.
 */
const Quaternion operator-(Quaternion lhs, const Quaternion& rhs);

/**
 *  @brief
 *      Multiplies a Quaternion by a scalar value.
 *
 *  @param [in] quaternion
 *      The Quaternion to multiply.
 *
 *  @param [in] scalar
 *      The scalar value to multiply by.
 *
 *  @return
 *      \a quaternion multiplied by \a scalar.
 */
const Quaternion operator*(Quaternion quaternion, float scalar);

/**
 *  @brief
 *      Multiplies a Quaternion by a scalar value.
 *
 *  @param [in] scalar
 *      The scalar value to multiply by.
 *
 *  @param [in] quaternion
 *      The Quaternion to multiply.
 *
 *  @return
 *      \a quaternion multiplied by \a scalar.
 */
const Quaternion operator*(float scalar, Quaternion quaternion);

/**
 *  @brief
 *      Multiplies two Quaternions.
 *
 *  @param [in] lhs
 *      The first Quaternion to multiply.
 *
 *  @param [in] rhs
 *      The second Quaternion to multiply.
 *
 *  @return
 *      The product of \a lhs and \a rhs.
 */
const Quaternion operator*(Quaternion lhs, const Quaternion& rhs);

/**
 *  @brief
 *      Multiplies a Quaternion by a Vector.
 *
 *  @param [in] quaternion
 *      The Quaternion to multiply.
 *
 *  @param [in] vector
 *      The Vector to multiply by.
 *
 *  @return
 *      \a quaternion multiplied by \a vector.
 */
const Quaternion operator*(Quaternion quaternion, const Vec3f& vector);

/**
 *  @brief
 *      Divides a Quaternion by a scalar value.
 *
 *  @param [in] quaternion
 *      The Quaternion to divide.
 *
 *  @param [in] scalar
 *      The scalar value to divide by.
 *
 *  @return
 *      \a quaternion divided by \a scalar.
 */
const Quaternion operator/(Quaternion quaternion, float scalar);

/**
 *  @brief
 *      Checks to see if two Quaternions are equal.
 *
 *  @param [in] lhs
 *      The first Quaternion to compare.
 *
 *  @param [in] rhs
 *      The second Quaternion to compare.
 *
 *  @retval true
 *      \a lhs and \a rhs are equal.
 *
 *  @retval false
 *      \a lhs and \a rhs are not equal.
 */
bool operator==(const Quaternion& lhs, const Quaternion& rhs);

/**
 *  @brief
 *      Checks to see if two Quaternions are not equal.
 *
 *  @param [in] lhs
 *      The first Quaternion to compare.
 *
 *  @param [in] rhs
 *      The second Quaternion to compare.
 *
 *  @retval true
 *      \a lhs and \a rhs are not equal.
 *
 *  @retval false
 *      \a lhs and \a rhs are equal.
 */
bool operator!=(const Quaternion& lhs, const Quaternion& rhs);

/**
 *  @brief
 *      Inserts a Quaternion into an output stream.
 *
 *  @param [in, out] stream
 *      The output stream.
 *
 *  @param [in] quaternion
 *      The Quaternion to insert.
 *
 *  @return
 *      \a stream.
 */
std::ostream& operator<<(std::ostream& stream, const Quaternion& quaternion);

/**
 *  @brief
 *      Finds the length of a Quaternion.
 *
 *  @param [in] quaternion
 *      The Quaternion to query.
 *
 *  @return
 *      The length of \a quaternion.
 */
float Length(const Quaternion& quaternion);

/**
 *  @brief
 *      Calculates a normalized Quaternion.
 *
 *  @param [in] quaternion
 *      The Quaternion to normalize.
 *
 *  @returns
 *      The normalized Quaternion.
 */
const Quaternion Normalize(Quaternion quaternion);

/**
 *  @brief
 *      Finds the conjugate of a Quaternion.
 *
 *  @param [in] quaternion
 *      The Quaternion to query.
 *
 *  @returns
 *      The conjugate of \a quaternion.
 */
const Quaternion Conjugate(const Quaternion& quaternion);

/**
 *  @brief
 *      Finds the rotation Matrix corresponding to
 *      the rotation described by a Quaternion.
 *
 *  @param [in] quaternion
 *      The Quaternion to convert.
 *
 *  @returns
 *      The rotation Matrix.
 */
const Mat4x4f GetRotationMatrix(const Quaternion& quaternion);

#endif /* Quaternion_hpp */
