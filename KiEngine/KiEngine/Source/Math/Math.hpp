/**
 *  @file
 *      Math.hpp
 *
 *  @brief
 *      A math library.
 */

#ifndef Math_hpp
#define Math_hpp

#include "Vector.hpp"
#include "Matrix.hpp"
#include "Quaternion.hpp"

/**
 *  @brief
 *      Defines mathematical constants and functions.
 */
namespace Math
{
    /**
     *  @brief
     *      The mathematical constant pi.
     */
    const float PI = 3.1415927;

    /**
     *  @brief
     *      Converts an angle to radians.
     *
     *  @param [in] angle
     *      The angle (in degrees) to convert.
     *
     *  @return
     *      \a angle in radians.
     */
    float ToRadians(float angle);

    /**
     *  @brief
     *      Converts an angle to degrees.
     *
     *  @param [in] angle
     *      The angle (in radians) to convert.
     *
     *  @return
     *      \a angle in degrees.
     */
    float ToDegrees(float angle);

    /**
     *  @brief
     *      Translates a vector.
     *
     *  @param [in] vector
     *      The vector to translate.
     *
     *  @param [in] x
     *      The amount to translate in the x-axis.
     *
     *  @param [in] y
     *      The amount to translate in the y-axis.
     *
     *  @param [in] z
     *      The amount to translate in the z-axis.
     *
     *  @return
     *      The translated vector.
     */
    Vec3f Translate(Vec3f vector, float x, float y, float z);

    /**
     *  @brief
     *      Translates a vector.
     *
     *  @param [in] vector
     *      The vector to translate.
     *
     *  @param [in] translation
     *      The amount to translate.
     *
     *  @return
     *      The translated vector.
     */
    Vec3f Translate(Vec3f vector, Vec3f translation);

    /**
     *  @brief
     *      Generates a translation matrix.
     *
     *  @param [in] x
     *      The amount to translate in the x-axis.
     *
     *  @param [in] y
     *      The amount to translate in the y-axis.
     *
     *  @param [in] z
     *      The amount to translate in the z-axis.
     *
     *  @return
     *      The translation matrix.
     */
    Mat4x4f Translation(float x, float y, float z);

    /**
     *  @brief
     *      Generates a translation matrix.
     *
     *  @param [in] translation
     *      The amount to translate.
     *
     *  @return
     *      The translation matrix.
     */
    Mat4x4f Translation(Vec3f translation);

    /**
     *  @brief
     *      Yaws a vector.
     *
     *  @param [in] vector
     *      The vector to yaw.
     *
     *  @param [in] angle
     *      The angle (in degrees) to yaw.
     *
     *  @return
     *      The rotated vector.
     *
     *  A yaw is a rotation about the y-axis.
     *  A negative angle yaws to the left; a positive angle yaws to the right.
     */
    Vec3f Yaw(Vec3f vector, float angle);

    /**
     *  @brief
     *      Pitches a vector.
     *
     *  @param [in] vector
     *      The vector to pitch.
     *
     *  @param [in] angle
     *      The angle (in degrees) to pitch.
     *
     *  @return
     *      The rotated vector.
     *
     *  A pitch is a rotation about the x-axis.
     *  A negative angle pitches up; a positive angle pitches down.
     */
    Vec3f Pitch(Vec3f vector, float angle);

    /**
     *  @brief
     *      Rolls a vector.
     *
     *  @param [in] vector
     *      The vector to roll.
     *
     *  @param [in] angle
     *      The angle (in degrees) to roll.
     *
     *  @return
     *      The rotated vector.
     *
     *  A roll is a rotation about the z-axis.
     *  A positive angle rolls to the left; a negative angle rolls to the right.
     */
    Vec3f Roll(Vec3f vector, float angle);

    /**
     *  @brief
     *      Rotates a vector about an axis.
     *
     *  @param [in] vector
     *      The vector to rotate.
     *
     *  @param [in] axis
     *      The axis of rotation.
     *
     *  @param [in] angle
     *      The angle (in degrees) to rotate.
     *
     *  @return
     *      The rotated vector.
     *
     *  When looking down the axis, a positive angle rotates anticlockwise;
     *  a negative angle rotates clockwise.
     */
    Vec3f Rotate(Vec3f vector, Vec3f axis, float angle);

    /**
     *  @brief
     *      Generates a yaw quaternion.
     *
     *  @param [in] angle
     *      The angle (in degrees) to yaw.
     *
     *  @return
     *      The yaw quaternion.
     *
     *  A yaw is a rotation about the y-axis.
     *  A negative angle yaws to the left; a positive angle yaws to the right.
     */
    Quaternion Yaw(float angle);

    /**
     *  @brief
     *      Generates a pitch quaternion.
     *
     *  @param [in] angle
     *      The angle (in degrees) to pitch.
     *
     *  @return
     *      The pitch quaternion.
     *
     *  A pitch is a rotation about the x-axis.
     *  A negative angle pitches up; a positive angle pitches down.
     */
    Quaternion Pitch(float angle);

    /**
     *  @brief
     *      Generates a roll quaternion.
     *
     *  @param [in] angle
     *      The angle (in degrees) to roll.
     *
     *  @return
     *      The roll quaternion.
     *
     *  A roll is a rotation about the z-axis.
     *  A positive angle rolls to the left; a negative angle rolls to the right.
     */
    Quaternion Roll(float angle);

    /**
     *  @brief
     *      Generates a rotation quaternion about an axis.
     *
     *  @param [in] axis
     *      The axis of rotation.
     *
     *  @param [in] angle
     *      The angle (in degrees) to rotate.
     *
     *  @return
     *      The rotation quaternion.
     *
     *  When looking down the axis, a positive angle rotates anticlockwise;
     *  a negative angle rotates clockwise.
     */
    Quaternion Rotation(Vec3f axis, float angle);

    /**
     *  @brief
     *      Scales a vector uniformly.
     *
     *  @param [in] vector
     *      The vector to scale.
     *
     *  @param [in] scale
     *      The amount to scale in all axes.
     *
     *  @return
     *      The scaled vector.
     */
    Vec3f Scale(Vec3f vector, float scale);

    /**
     *  @brief
     *      Scales a vector.
     *
     *  @param [in] vector
     *      The vector to scale.
     *
     *  @param [in] x
     *      The amount to scale in the x-axis.
     *
     *  @param [in] y
     *      The amount to scale in the y-axis.
     *
     *  @param [in] z
     *      The amount to scale in the z-axis.
     *
     *  @return
     *      The scaled vector.
     */
    Vec3f Scale(Vec3f vector, float x, float y, float z);

    /**
     *  @brief
     *      Scales a vector.
     *
     *  @param [in] vector
     *      The vector to scale.
     *
     *  @param [in] scale
     *      The amount to scale.
     *
     *  @return
     *      The scaled vector.
     */
    Vec3f Scale(Vec3f vector, Vec3f scale);

    /**
     *  @brief
     *      Generates a uniform scale matrix.
     *
     *  @param [in] scale
     *      The amount to scale in all axes.
     *
     *  @return
     *      The scale matrix.
     */
    Mat4x4f Scale(float scale);

    /**
     *  @brief
     *      Generates a scale matrix.
     *
     *  @param [in] x
     *      The amount to scale in the x-axis.
     *
     *  @param [in] y
     *      The amount to scale in the y-axis.
     *
     *  @param [in] z
     *      The amount to scale in the z-axis.
     *
     *  @return
     *      The scale matrix.
     */
    Mat4x4f Scale(float x, float y, float z);

    /**
     *  @brief
     *      Generates a scale matrix.
     *
     *  @param [in] scale
     *      The amount to scale.
     *
     *  @return
     *      The scale matrix.
     */
    Mat4x4f Scale(Vec3f scale);
}

#endif /* Math_hpp */
