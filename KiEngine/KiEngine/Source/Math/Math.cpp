/**
 *  @file
 *      Math.cpp
 *
 *  @brief
 *      A math library.
 */

#include "Math.hpp"

#include "Quaternion.hpp"

namespace Math
{

    float ToRadians(float angle)
    {
        return angle * PI / 180;
    }

    float ToDegrees(float angle)
    {
        return angle * 180 / PI;
    }

    Vec3f Translate(Vec3f vector, float x, float y, float z)
    {
        return vector + Vec3f(x, y, z);
    }

    Vec3f Translate(Vec3f vector, Vec3f translation)
    {
        return Translate(vector, translation.x, translation.y, translation.z);
    }

    Mat4x4f Translation(float x, float y, float z)
    {
        return Mat4x4f(
        {
            1,  0,  0,  x,
            0,  1,  0,  y,
            0,  0,  1,  z,
            0,  0,  0,  1
        });
    }

    Mat4x4f Translation(Vec3f translation)
    {
        return Translation(translation.x, translation.y, translation.z);
    }

    Vec3f Yaw(Vec3f vector, float angle)
    {
        Quaternion yaw = Yaw(angle);
        Quaternion result = yaw * vector * Conjugate(yaw);

        return Vec3f(result.x, result.y, result.z);
    }

    Vec3f Pitch(Vec3f vector, float angle)
    {
        Quaternion pitch = Pitch(angle);
        Quaternion result = pitch * vector * Conjugate(pitch);

        return Vec3f(result.x, result.y, result.z);
    }

    Vec3f Roll(Vec3f vector, float angle)
    {
        Quaternion roll = Roll(angle);
        Quaternion result = roll * vector * Conjugate(roll);

        return Vec3f(result.x, result.y, result.z);
    }

    Vec3f Rotate(Vec3f vector, Vec3f axis, float angle)
    {
        Quaternion rotation = Rotation(axis, angle);
        Quaternion result = rotation * vector * Conjugate(rotation);

        return Vec3f(result.x, result.y, result.z);
    }

    Quaternion Yaw(float angle)
    {
        angle = ToRadians(angle / 2);

        return Quaternion(cosf(angle),
                          0,
                          sinf(angle),
                          0);
    }

    Quaternion Pitch(float angle)
    {
        angle = ToRadians(angle / 2);

        return Quaternion(cosf(angle),
                          sinf(angle),
                          0,
                          0);
    }

    Quaternion Roll(float angle)
    {
        angle = ToRadians(angle / 2);

        return Quaternion(cosf(angle),
                          0,
                          0,
                          sinf(angle));
    }

    Quaternion Rotation(Vec3f axis, float angle)
    {
        angle = ToRadians(angle / 2);
        float sinAngle = sinf(angle);
        float cosAngle = cosf(angle);

        return Quaternion(cosAngle,
                          axis.x * sinAngle,
                          axis.y * sinAngle,
                          axis.z * sinAngle);
    }

    Vec3f Scale(Vec3f vector, float scale)
    {
        return Scale(vector, scale, scale, scale);
    }

    Vec3f Scale(Vec3f vector, float x, float y, float z)
    {
        return vector * Vec3f(x, y, z);
    }

    Vec3f Scale(Vec3f vector, Vec3f scale)
    {
        return Scale(vector, scale.x, scale.y, scale.z);
    }

    Mat4x4f Scale(float scale)
    {
        return Scale(scale, scale, scale);
    }

    Mat4x4f Scale(float x, float y, float z)
    {
        return Mat4x4f(
        {
            x,  0,  0,  0,
            0,  y,  0,  0,
            0,  0,  z,  0,
            0,  0,  0,  1
        });
    }

    Mat4x4f Scale(Vec3f scale)
    {
        return Scale(scale.x, scale.y, scale.z);
    }

}
