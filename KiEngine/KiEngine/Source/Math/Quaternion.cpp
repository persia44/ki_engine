/**
 *  @file
 *      Quaternion.cpp
 *
 *  @brief
 *      Contains a Quaternion type along with related functions.
 */

#include "Quaternion.hpp"

#include <cmath>
#include <iostream>

Quaternion::Quaternion() :
    Quaternion(1, 0, 0, 0)
{
}

Quaternion::Quaternion(float w, float x, float y, float z) :
    w(w), x(x), y(y), z(z)
{
}

/**
 *  Each component of \a rhs is added to the corresponding component of \a lhs.
 */
Quaternion& operator+=(Quaternion& lhs, const Quaternion& rhs)
{
    lhs.w += rhs.w;
    lhs.x += rhs.x;
    lhs.y += rhs.y;
    lhs.z += rhs.z;

    return lhs;
}

/**
 *  Each component of \a lhs is subtracted by the corresponding component of \a rhs.
 */
Quaternion& operator-=(Quaternion& lhs, const Quaternion& rhs)
{
    lhs.w -= rhs.w;
    lhs.x -= rhs.x;
    lhs.y -= rhs.y;
    lhs.z -= rhs.z;

    return lhs;
}

/**
 *  Each component of \a quaternion is multiplied by \a scalar.
 */
Quaternion& operator*=(Quaternion& quaternion, float scalar)
{
    quaternion.w *= scalar;
    quaternion.x *= scalar;
    quaternion.y *= scalar;
    quaternion.z *= scalar;

    return quaternion;
}

/**
 *  The product of two Quaternions is:
 *      \f[
 *          (l_w r_w - l_x r_x - l_y r_y - l_z r_z) +
 *          (l_w r_x + l_x r_w + l_y r_z - l_z r_y)i +
 *          (l_w r_y + l_y r_w + l_z r_x - l_x r_z)j +
 *          (l_w r_z + l_z r_w + l_x r_y - l_y r_x)k
 *      \f]
 *  where:
 *      \f[
 *          lhs = l_w + l_x i + l_y j + l_z k
 *      \f]
 *      \f[
 *          rhs = r_w + r_x i + r_y j + r_z k
 *      \f]
 */
Quaternion& operator*=(Quaternion& lhs, const Quaternion& rhs)
{
    Quaternion result;

    result.w = lhs.w * rhs.w - lhs.x * rhs.x - lhs.y * rhs.y - lhs.z * rhs.z;
    result.x = lhs.w * rhs.x + lhs.x * rhs.w + lhs.y * rhs.z - lhs.z * rhs.y;
    result.y = lhs.w * rhs.y + lhs.y * rhs.w + lhs.z * rhs.x - lhs.x * rhs.z;
    result.z = lhs.w * rhs.z + lhs.z * rhs.w + lhs.x * rhs.y - lhs.y * rhs.x;

    lhs = result;

    return lhs;
}

/**
 *  The product of a Quaternion \f$ q \f$ and a Vector \f$ v \f$ equivalent to:
 *      \f[
 *          q \times w
 *      \f]
 *  where \f$ w \f$ is a Quaternion defined as \f$ (0, v.x, v.y, v.z) \f$.
 */
Quaternion& operator*=(Quaternion& quaternion, const Vec3f& vector)
{
    Quaternion result;

    result.w = -quaternion.x * vector.x - quaternion.y * vector.y - quaternion.z * vector.z;
    result.x =  quaternion.w * vector.x + quaternion.y * vector.z - quaternion.z * vector.y;
    result.y =  quaternion.w * vector.y + quaternion.z * vector.x - quaternion.x * vector.z;
    result.z =  quaternion.w * vector.z + quaternion.x * vector.y - quaternion.y * vector.x;

    quaternion = result;

    return quaternion;
}

/**
 *  Each component of \a quaternion is divided by \a scalar.
 */
Quaternion& operator/=(Quaternion& quaternion, float scalar)
{
    quaternion.w /= scalar;
    quaternion.x /= scalar;
    quaternion.y /= scalar;
    quaternion.z /= scalar;

    return quaternion;
}

/**
 *  The Quaternions are added component-wise.
 */
const Quaternion operator+(Quaternion lhs, const Quaternion& rhs)
{
    lhs += rhs;
    return lhs;
}

/**
 *  The Quaternions are subtracted component-wise.
 */
const Quaternion operator-(Quaternion lhs, const Quaternion& rhs)
{
    lhs -= rhs;
    return lhs;
}

/**
 *  Each component of \a quaternion is multiplied by \a scalar.
 */
const Quaternion operator*(Quaternion quaternion, float scalar)
{
    quaternion *= scalar;
    return quaternion;
}

/**
 *  Each component of \a quaternion is multiplied by \a scalar.
 */
const Quaternion operator*(float scalar, Quaternion quaternion)
{
    return quaternion * scalar;
}

/**
 *  The product of two Quaternions is:
 *      \f[
 *          (l_w r_w - l_x r_x - l_y r_y - l_z r_z) +
 *          (l_w r_x + l_x r_w + l_y r_z - l_z r_y)i +
 *          (l_w r_y + l_y r_w + l_z r_x - l_x r_z)j +
 *          (l_w r_z + l_z r_w + l_x r_y - l_y r_x)k
 *      \f]
 *  where:
 *      \f[
 *          lhs = l_w + l_x i + l_y j + l_z k
 *      \f]
 *      \f[
 *          rhs = r_w + r_x i + r_y j + r_z k
 *      \f]
 */
const Quaternion operator*(Quaternion lhs, const Quaternion& rhs)
{
    lhs *= rhs;
    return lhs;
}

/**
 *  The product of a Quaternion \f$ q \f$ and a Vector \f$ v \f$ equivalent to:
 *      \f[
 *          q \times w
 *      \f]
 *  where \f$ w \f$ is a Quaternion defined as \f$ (0, v.x, v.y, v.z) \f$.
 */
const Quaternion operator*(Quaternion quaternion, const Vec3f& vector)
{
    quaternion *= vector;
    return quaternion;
}

/**
 *  Each component of \a quaternion is divided by \a scalar.
 */
const Quaternion operator/(Quaternion quaternion, float scalar)
{
    quaternion /= scalar;
    return quaternion;
}

/**
 *  Two Quaternions are equal if and only if each of their components are equal.
 */
bool operator==(const Quaternion& lhs, const Quaternion& rhs)
{
    return lhs.w == rhs.w &&
           lhs.x == rhs.x &&
           lhs.y == rhs.y &&
           lhs.z == rhs.z;
}

/**
 *  Two Quaternions are not equal if any their components are not equal.
 *
 *  This is the inverse of operator==, and is defined as such.
 */
bool operator!=(const Quaternion& lhs, const Quaternion& rhs)
{
    return !(lhs == rhs);
}

std::ostream& operator<<(std::ostream& stream, const Quaternion& quaternion)
{
    stream << quaternion.w << " + " <<
              quaternion.x << "i + " <<
              quaternion.y << "j + " <<
              quaternion.z << "k";

    return stream;
}

/**
 *  The length of a Quaternion is defined as:
 *      \f[
 *          \sqrt{w^2 + x^2 + y^2 + z^2}
 *      \f]
 */
float Length(const Quaternion& quaternion)
{
    return std::sqrt(quaternion.w * quaternion.w +
                     quaternion.x * quaternion.x +
                     quaternion.y * quaternion.y +
                     quaternion.z * quaternion.z);
}

/**
 *  A normalized Quaternion is a Quaternion with unit length (i.e. 1).
 *  This is achieved by dividing each component by its length.
 */
const Quaternion Normalize(Quaternion quaternion)
{
    float length = Length(quaternion);

    quaternion.w /= length;
    quaternion.x /= length;
    quaternion.y /= length;
    quaternion.z /= length;

    return quaternion;
}

const Quaternion Conjugate(const Quaternion& quaternion)
{
    return Quaternion(quaternion.w, -quaternion.x, -quaternion.y, -quaternion.z);
}

const Mat4x4f GetRotationMatrix(const Quaternion& quaternion)
{
    float wx = quaternion.w * quaternion.x;
    float wy = quaternion.w * quaternion.y;
    float wz = quaternion.w * quaternion.z;
    float xx = quaternion.x * quaternion.x;
    float xy = quaternion.x * quaternion.y;
    float xz = quaternion.x * quaternion.z;
    float yy = quaternion.y * quaternion.y;
    float yz = quaternion.y * quaternion.z;
    float zz = quaternion.z * quaternion.z;

    return Mat4x4f(
    {
        1 - 2 * yy - 2 * zz,    2 * xy - 2 * wz,        2 * xz + 2 * wy,        0,
        2 * xy + 2 * wz,        1 - 2 * xx - 2 * zz,    2 * yz + 2 * wx,        0,
        2 * xz - 2 * wy,        2 * yz - 2 * wx,        1 - 2 * xx - 2 * yy,    0,
        0,                      0,                      0,                      1
    });
}
