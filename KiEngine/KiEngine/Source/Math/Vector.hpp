/**
 *  @file
 *      Vector.hpp
 *
 *  @brief
 *      Contains a Vector type along with related functions.
 */

#ifndef Vector_hpp
#define Vector_hpp

#include <cmath>
#include <iostream>

/**
 *  @brief
 *      A mathematical vector which can be instantiated with any size or type.
 *
 *  @tparam Dimensions
 *      The size of the Vector (number of dimensions).
 *
 *  @tparam Type
 *      The type of each value in the Vector.
 */
template <int Dimensions, typename Type>
struct Vector;

/**
 *  @name
 *      Common Vector types.
 */
/// @{
typedef Vector<2, float> Vec2f;
typedef Vector<3, float> Vec3f;
typedef Vector<4, float> Vec4f;

typedef Vector<2, int> Vec2i;
typedef Vector<3, int> Vec3i;
typedef Vector<4, int> Vec4i;
/// @}

/**
 *  @brief
 *      Sets the values of a Vector's components.
 *
 *  @param [in, out] vector
 *      The Vector to modify.
 *
 *  @param [in] components
 *      The values to set.
 */
template <int Dimensions, typename Type>
void SetComponents(Vector<Dimensions, Type>& vector, std::initializer_list<Type> components);

/**
 *  @brief
 *      Sets all of a Vector's components to a given value.
 *
 *  @param [in, out] vector
 *      The Vector to modify.
 *
 *  @param [in] value
 *      The value to set.
 */
template <int Dimensions, typename Type>
void FillComponents(Vector<Dimensions, Type>& vector, const Type& value);

/**
 *  @brief
 *      Gets a reference to a Vector component.
 *
 *  @param [in] vector
 *      The Vector to query.
 *
 *  @param [in] i
 *      The index of the component.
 *
 *  @returns
 *      A reference to component \a i of \a vector.
 */
template <int Dimensions, typename Type>
Type& GetComponent(Vector<Dimensions, Type>& vector, const int i);

/**
 *  @brief
 *      Gets a const reference to a Vector component.
 *
 *  @param [in] vector
 *      The Vector to query.
 *
 *  @param [in] i
 *      The index of the component.
 *
 *  @returns
 *      A const reference to component \a i of \a vector.
 */
template <int Dimensions, typename Type>
const Type& GetComponent(const Vector<Dimensions, Type>& vector, const int i);

/**
 *  @brief
 *      Finds the length of a Vector.
 *
 *  @param [in] vector
 *      The Vector to query.
 *
 *  @returns
 *      The length of \a vector.
 */
template <int Dimensions, typename Type>
const Type Length(const Vector<Dimensions, Type>& vector);

/**
 *  @brief
 *      Calculates a normalized Vector.
 *
 *  @param [in] vector
 *      The Vector to normalize.
 *
 *  @returns
 *      The normalized Vector.
 */
template <int Dimensions, typename Type>
const Vector<Dimensions, Type> Normalize(Vector<Dimensions, Type> vector);

/**
 *  @brief
 *      Calculates the dot product of two Vectors.
 *
 *  @param [in] lhs
 *      The first Vector in the dot product.
 *
 *  @param [in] rhs
 *      The second Vector in the dot product.
 *
 *  @returns
 *      The dot product of \a lhs and \a rhs.
 */
template <int Dimensions, typename Type>
const Type Dot(Vector<Dimensions, Type> lhs, const Vector<Dimensions, Type>& rhs);

/**
 *  @brief
 *      Calculates the cross product of two Vectors.
 *
 *  @param [in] lhs
 *      The first Vector in the cross product.
 *
 *  @param [in] rhs
 *      The second Vector in the cross product.
 *
 *  @returns
 *      The cross product of \a lhs and \a rhs.
 */
template <typename Type>
const Vector<3, Type> Cross(const Vector<3, Type>& lhs, const Vector<3, Type>& rhs);

/*============================================================================*/

template <int Dimensions, typename Type>
struct Vector
{
    /**
     *  @brief
     *      The Vector components.
     */
    Type components[Dimensions];

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] value
     *      The value to fill the Vector with. Defaults to 0.
     *
     *  Constructs a Vector and initializes all of its
     *  components to \a value.
     */
    Vector(const Type& value = 0)
    {
        FillComponents(*this, value);
    }

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] components
     *      The values to fill the Vector with.
     *
     *  Constructs a Vector and initializes all of its
     *  components with the values in \a components.
     */
    Vector(std::initializer_list<Type> components)
    {
        SetComponents(*this, components);
    }

    /**
     *  @brief
     *      Subscript operator.
     *
     *  @param [in] i
     *      Index of the component to access.
     *
     *  @return
     *      A reference to component \a i.
     */
    Type& operator[](const int i)
    {
        return GetComponent(*this, i);
    }

    /**
     *  @brief
     *      Const subscript operator.
     *
     *  @param [in] i
     *      Index of the component to access.
     *
     *  @return
     *      A const reference to component \a i.
     */
    const Type& operator[](const int i) const
    {
        return GetComponent(*this, i);
    }
};

/**
 *  @brief
 *      A two dimensional Vector which supports more specialized
 *      features than a generic Vector.
 *
 *  @tparam Type
 *      The type of each value in the Vector.
 */
template <typename Type>
struct Vector<2, Type>
{
    /**
     *  @brief
     *      The Vector components.
     *
     *  This is implemented as a union holding both an array and an unnamed
     *  struct with two named members. This allows use of \a x and \a y to
     *  refer to the Vector components while remaining compatible with the
     *  generic Vector.
     */
    union
    {
        /// @cond
        Type components[2];
        struct
        {
            Type x, y;
        };
        /// @endcond
    };

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] value
     *      The value to fill the Vector with. Defaults to 0.
     *
     *  Constructs a Vector and initializes all of its
     *  components to \a value.
     */
    Vector(const Type& value = 0)
    {
        FillComponents(*this, value);
    }

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] components
     *      The values to fill the Vector with.
     *
     *  Constructs a Vector and initializes all of its
     *  components with the values in \a components.
     */
    Vector(std::initializer_list<Type> components)
    {
        SetComponents(*this, components);
    }

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] x
     *      Value to set the \a x component to.
     *
     *  @param [in] y
     *      Value to set the \a y component to.
     *
     *  Constructs a Vector with the value (\a x, \a y).
     */
    Vector(Type x, Type y) : x(x), y(y) {}

    /**
     *  @brief
     *      Subscript operator
     *
     *  @param [in] i
     *      Index of the component to access.
     *
     *  @return
     *      A reference to component \a i.
     */
    Type& operator[](const int i)
    {
        return GetComponent(*this, i);
    }

    /**
     *  @brief
     *      Const subscript operator.
     *
     *  @param [in] i
     *      Index of the component to access.
     *
     *  @return
     *      A const reference to component \a i.
     */
    const Type& operator[](const int i) const
    {
        return GetComponent(*this, i);
    }
};

/**
 *  @brief
 *      A three dimensional Vector which supports more specialized
 *      features than a generic Vector.
 *
 *  @tparam Type
 *      The type of each value in the Vector.
 */
template <typename Type>
struct Vector<3, Type>
{
    /**
     *  @brief
     *      The Vector components.
     *
     *  This is implemented as a union holding several different types with
     *  different names. This allows use of (\a x, \a y, \a z) or
     *  (\a r, \a g, \a b) to refer to the Vector components while remaining
     *  compatible with the generic Vector. It also allows some degree of swizzling.
     */
    union
    {
        /// @cond
        Type components[3];
        struct
        {
            Type x, y, z;
        };
        Vector<2, Type> xy;
        struct
        {
            Type r, g, b;
        };
        /// @endcond
    };

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] value
     *      The value to fill the Vector with. Defaults to 0.
     *
     *  Constructs a Vector and initializes all of its
     *  components to \a value.
     */
    Vector(const Type& value = 0)
    {
        FillComponents(*this, value);
    }

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] components
     *      The values to fill the Vector with.
     *
     *  Constructs a Vector and initializes all of its
     *  components with the values in \a components.
     */
    Vector(std::initializer_list<Type> components)
    {
        SetComponents(*this, components);
    }

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] x
     *      Value to set the \a x component to.
     *
     *  @param [in] y
     *      Value to set the \a y component to.
     *
     *  @param [in] z
     *      Value to set the \a z component to.
     *
     *  Constructs a Vector with the value (\a x, \a y, \a z).
     */
    Vector(Type x, Type y, Type z) : x(x), y(y), z(z) {}

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] vector
     *      Two dimensional Vector used to set the \a x and \a y components.
     *
     *  @param [in] z
     *      Value to set the \a z component to.
     *
     *  Constructs a Vector with the value (\a vector.x, \a vector.y, \a z).
     */
    Vector(Vector<2, Type> vector, Type z) : Vector(vector.x, vector.y, z) {}

    /**
     *  @brief
     *      Subscript operator
     *
     *  @param [in] i
     *      Index of the component to access.
     *
     *  @return
     *      A reference to component \a i.
     */
    Type& operator[](const int i)
    {
        return GetComponent(*this, i);
    }

    /**
     *  @brief
     *      Const subscript operator.
     *
     *  @param [in] i
     *      Index of the component to access.
     *
     *  @return
     *      A const reference to component \a i.
     */
    const Type& operator[](const int i) const
    {
        return GetComponent(*this, i);
    }
};

/**
 *  @brief
 *      A four dimensional Vector which supports more specialized
 *      features than a generic Vector.
 *
 *  @tparam Type
 *      The type of each value in the Vector.
 */
template <typename Type>
struct Vector<4, Type>
{
    /**
     *  @brief
     *      The Vector components.
     *
     *  This is implemented as a union holding several different types with
     *  different names. This allows use of (\a x, \a y, \a z, \a w) or
     *  (\a r, \a g, \a b, \a a) to refer to the Vector components while remaining
     *  compatible with the generic Vector. It also allows some degree of swizzling.
     */
    union
    {
        /// @cond
        Type components[4];
        struct
        {
            Type x, y, z, w;
        };
        Vector<2, Type> xy;
        Vector<3, Type> xyz;
        struct
        {
            Type r, g, b, a;
        };
        Vector<3, Type> rgb;
        /// @endcond
    };

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] value
     *      The value to fill the Vector with. Defaults to 0.
     *
     *  Constructs a Vector and initializes all of its
     *  components to \a value.
     */
    Vector(const Type& value = 0)
    {
        FillComponents(*this, value);
    }

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] components
     *      The values to fill the Vector with.
     *
     *  Constructs a Vector and initializes all of its
     *  components with the values in \a components.
     */
    Vector(std::initializer_list<Type> components)
    {
        SetComponents(*this, components);
    }

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] x
     *      Value to set the \a x component to.
     *
     *  @param [in] y
     *      Value to set the \a y component to.
     *
     *  @param [in] z
     *      Value to set the \a z component to.
     *
     *  @param [in] w
     *      Value to set the \a w component to.
     *
     *  Constructs a Vector with the value (\a x, \a y, \a z, \a w).
     */
    Vector(Type x, Type y, Type z, Type w) : x(x), y(y), z(z), w(w) {}

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] vector
     *      Three dimensional vector used to set the \a x, \a y and \a z components.
     *
     *  @param [in] w
     *      Value to set the \a w component to.
     *
     *  Constructs a Vector with the value (\a vector.x, \a vector.y, \a vector.z, \a w).
     */
    Vector(Vector<3, Type> vector, Type w) : Vector(vector.x, vector.y, vector.z, w) {}

    /**
     *  @brief
     *      Subscript operator
     *
     *  @param [in] i
     *      Index of the component to access.
     *
     *  @return
     *      A reference to component \a i.
     */
    Type& operator[](const int i)
    {
        return GetComponent(*this, i);
    }

    /**
     *  @brief
     *      Const subscript operator.
     *
     *  @param [in] i
     *      Index of the component to access.
     *
     *  @return
     *      A const reference to component \a i.
     */
    const Type& operator[](const int i) const
    {
        return GetComponent(*this, i);
    }
};

/**
 *  @brief
 *      Negates a Vector
 *
 *  @param [in] vector
 *      The Vector to negate.
 *
 *  @return
 *      \a vector negated.
 *
 *  The Vector is negated component-wise.
 */
template <int Dimensions, typename Type>
const Vector<Dimensions, Type> operator-(Vector<Dimensions, Type> vector)
{
    for (Type& component : vector.components)
    {
        component = -component;
    }

    return vector;
}

/**
 *  @brief
 *      Adds two Vectors and assigns the result to the left hand side Vector.
 *
 *  @param [in, out] lhs
 *      The first Vector to add and have the result assigned to.
 *
 *  @param [in] rhs
 *      The second Vector to add.
 *
 *  @return
 *      \a lhs after the add operation.
 *
 *  Each component of \a rhs is added to the corresponding component of \a lhs.
 */
template <int Dimensions, typename Type>
Vector<Dimensions, Type>& operator+=(Vector<Dimensions, Type>& lhs, const Vector<Dimensions, Type>& rhs)
{
    for (int i = 0; i < Dimensions; i++)
    {
        lhs[i] += rhs[i];
    }
    return lhs;
}

/**
 *  @brief
 *      Subtracts two Vectors and assigns the result to the left hand side Vector.
 *
 *  @param [in, out] lhs
 *      The Vector to subtract from and have the result assigned to.
 *
 *  @param [in] rhs
 *      The Vector to subtract by.
 *
 *  @return
 *      \a lhs after the subtract operation.
 *
 *  Each component of \a lhs is subtracted by the corresponding component of \a rhs.
 */
template <int Dimensions, typename Type>
Vector<Dimensions, Type>& operator-=(Vector<Dimensions, Type>& lhs, const Vector<Dimensions, Type>& rhs)
{
    for (int i = 0; i < Dimensions; i++)
    {
        lhs[i] -= rhs[i];
    }
    return lhs;
}

/**
 *  @brief
 *      Multiplies a Vector by a scalar value and assigns the result to the Vector.
 *
 *  @param [in, out] vector
 *      The Vector to multiply and have the result assigned to.
 *
 *  @param [in] scalar
 *      The scalar value to multiply by.
 *
 *  @return
 *      \a vector after the multiply operation.
 *
 *  Each component of \a vector is multiplied by \a scalar.
 */
template <int Dimensions, typename Type>
Vector<Dimensions, Type>& operator*=(Vector<Dimensions, Type>& vector, const Type& scalar)
{
    for (Type& component : vector.components)
    {
        component *= scalar;
    }
    return vector;
}

/**
 *  @brief
 *      Multiplies two Vectors and assigns the result to the left hand side Vector.
 *
 *  @param [in, out] lhs
 *      The first Vector to multiply and have the result assigned to.
 *
 *  @param [in] rhs
 *      The second Vector to multiply.
 *
 *  @return
 *      \a lhs after the multiply operation.
 *
 *  Each component of \a lhs is multiplied by the corresponding component of \a rhs.
 */
template <int Dimensions, typename Type>
Vector<Dimensions, Type>& operator*=(Vector<Dimensions, Type>& lhs, const Vector<Dimensions, Type>& rhs)
{
    for (int i = 0; i < Dimensions; i++)
    {
        lhs[i] *= rhs[i];
    }
    return lhs;
}

/**
 *  @brief
 *      Divides a Vector by a scalar value and assigns the result to the Vector.
 *
 *  @param [in, out] vector
 *      The Vector to divide and have the result assigned to.
 *
 *  @param [in] scalar
 *      The scalar value to divide by.
 *
 *  @return
 *      \a vector after the divide operation.
 *
 *  Each component of \a vector is divided by \a scalar.
 */
template <int Dimensions, typename Type>
Vector<Dimensions, Type>& operator/=(Vector<Dimensions, Type>& vector, const Type& scalar)
{
    for (Type& component : vector.components)
    {
        component /= scalar;
    }
    return vector;
}

/**
 *  @brief
 *      Adds two Vectors.
 *
 *  @param [in] lhs
 *      The first Vector to add.
 *
 *  @param [in] rhs
 *      The second Vector to add.
 *
 *  @return
 *      The sum of \a lhs and \a rhs.
 *
 *  The Vectors are added component-wise.
 */
template <int Dimensions, typename Type>
const Vector<Dimensions, Type> operator+(Vector<Dimensions, Type> lhs, const Vector<Dimensions, Type>& rhs)
{
    lhs += rhs;
    return lhs;
}

/**
 *  @brief
 *      Subtracts two Vectors.
 *
 *  @param [in] lhs
 *      The Vector to subtract from.
 *
 *  @param [in] rhs
 *      The Vector to subtract by.
 *
 *  @return
 *      \a lhs subtracted by \a rhs.
 *
 *  The Vectors are subtracted component-wise.
 */
template <int Dimensions, typename Type>
const Vector<Dimensions, Type> operator-(Vector<Dimensions, Type> lhs, const Vector<Dimensions, Type>& rhs)
{
    lhs -= rhs;
    return lhs;
}

/**
 *  @brief
 *      Multiplies a Vector by a scalar value.
 *
 *  @param [in] vector
 *      The Vector to multiply.
 *
 *  @param [in] scalar
 *      The scalar value to multiply by.
 *
 *  @return
 *      \a vector multiplied by \a scalar.
 *
 *  Each component of \a vector is multiplied by \a scalar.
 */
template <int Dimensions, typename Type>
const Vector<Dimensions, Type> operator*(Vector<Dimensions, Type> vector, const Type& scalar)
{
    vector *= scalar;
    return vector;
}

/**
 *  @brief
 *      Multiplies a Vector by a scalar value.
 *
 *  @param [in] scalar
 *      The scalar value to multiply by.
 *
 *  @param [in] vector
 *      The Vector to multiply.
 *
 *  @return
 *      \a vector multiplied by \a scalar.
 *
 *  Each component of \a vector is multiplied by \a scalar.
 */
template <int Dimensions, typename Type>
const Vector<Dimensions, Type> operator*(const Type& scalar, Vector<Dimensions, Type> vector)
{
    return vector * scalar;
}

/**
 *  @brief
 *      Multiplies two Vectors.
 *
 *  @param [in] lhs
 *      The first Vector to multiply.
 *
 *  @param [in] rhs
 *      The second Vector to multiply.
 *
 *  @return
 *      The product of \a lhs and \a rhs.
 *
 *  The Vectors are multiplied component-wise.
 */
template <int Dimensions, typename Type>
const Vector<Dimensions, Type> operator*(Vector<Dimensions, Type> lhs, const Vector<Dimensions, Type>& rhs)
{
    lhs *= rhs;
    return lhs;
}

/**
 *  @brief
 *      Divides a Vector by a scalar value.
 *
 *  @param [in] vector
 *      The Vector to divide.
 *
 *  @param [in] scalar
 *      The scalar value to divide by.
 *
 *  @return
 *      \a vector divided by \a scalar.
 *
 *  Each component of \a vector is divided by \a scalar.
 */
template <int Dimensions, typename Type>
const Vector<Dimensions, Type> operator/(Vector<Dimensions, Type> vector, const Type& scalar)
{
    vector /= scalar;
    return vector;
}

/**
 *  @brief
 *      Checks to see if two Vectors are equal.
 *
 *  @param [in] lhs
 *      The first Vector to compare.
 *
 *  @param [in] rhs
 *      The second Vector to compare.
 *
 *  @retval true
 *      \a lhs and \a rhs are equal.
 *
 *  @retval false
 *      \a lhs and \a rhs are not equal.
 *
 *  Two Vectors are equal if and only if each of their components are equal.
 */
template <int Dimensions, typename Type>
bool operator==(const Vector<Dimensions, Type>& lhs, const Vector<Dimensions, Type>& rhs)
{
    return memcmp(lhs.components, rhs.components, sizeof(lhs)) == 0;
}

/**
 *  @brief
 *      Checks to see if two Vectors are not equal.
 *
 *  @param [in] lhs
 *      The first Vector to compare.
 *
 *  @param [in] rhs
 *      The second Vector to compare.
 *
 *  @retval true
 *      \a lhs and \a rhs are not equal.
 *
 *  @retval false
 *      \a lhs and \a rhs are equal.
 *
 *  Two Vectors are not equal if any their components are not equal.
 *
 *  This is the inverse of operator==, and is defined as such.
 */
template <int Dimensions, typename Type>
bool operator!=(const Vector<Dimensions, Type>& lhs, const Vector<Dimensions, Type>& rhs)
{
    return !(lhs == rhs);
}

/**
 *  @brief
 *      Inserts a Vector into an output stream.
 *
 *  @param [in, out] stream
 *      The output stream.
 *
 *  @param [in] vector
 *      The Vector to insert.
 *
 *  @return
 *      \a stream.
 */
template <int Dimensions, typename Type>
std::ostream& operator<<(std::ostream& stream, const Vector<Dimensions, Type>& vector)
{
    stream << "(" << vector[0];
    for (int i = 1; i < Dimensions; i++)
    {
        stream << ", " << vector[i];
    }
    stream << ")";

    return stream;
}

/**
 *  Assigns \a components to \a vector.components.
 */
template <int Dimensions, typename Type>
void SetComponents(Vector<Dimensions, Type>& vector, std::initializer_list<Type> components)
{
    KI_ASSERT(components.size() == Dimensions,
              components.size() << " components was provided; expected " << Dimensions);

    int i = 0;
    for (Type component : components)
    {
        vector[i++] = component;
    }
}

/**
 *  Fills \a vector.components with \a value.
 */
template <int Dimensions, typename Type>
void FillComponents(Vector<Dimensions, Type>& vector, const Type& value)
{
    for (Type& component : vector.components)
    {
        component = value;
    }
}

/**
 *  Simply accesses \a vector.components at \a i.
 */
template <int Dimensions, typename Type>
Type& GetComponent(Vector<Dimensions, Type>& vector, const int i)
{
    return vector.components[i];
}

/**
 *  Simply accesses \a vector.components at \a i.
 */
template <int Dimensions, typename Type>
const Type& GetComponent(const Vector<Dimensions, Type>& vector, const int i)
{
    return vector.components[i];
}

/**
 *  The length of a Vector is defined as:
 *      \f[
 *          \sqrt{\sum_{i=0}^{D} vector[i]^2}
 *      \f]
 *  where \f$D\f$ is the number of dimensions in \f$vector\f$.
 */
template <int Dimensions, typename Type>
const Type Length(const Vector<Dimensions, Type>& vector)
{
    Type sumSquares = 0;
    for (const Type& component : vector.components)
    {
        sumSquares += std::pow(component, 2);
    }
    return std::sqrt(sumSquares);
}

/**
 *  A normalized Vector is a Vector with unit length (i.e. 1).
 *  This is achieved by dividing each component of \a vector by its length.
 */
template <int Dimensions, typename Type>
const Vector<Dimensions, Type> Normalize(Vector<Dimensions, Type> vector)
{
    vector /= Length(vector);
    return vector;
}

/**
 *  The dot product of two Vectors is defined by:
 *      \f[
 *          \sum_{i=0}^{D-1} lhs[i] \times rhs[i]
 *      \f]
 *  where \f$D\f$ is the number of dimensions in \f$lhs\f$ and \f$rhs\f$.
 *
 *  \note \a lhs and \a rhs must have the same number of dimensions.
 */
template <int Dimensions, typename Type>
const Type Dot(Vector<Dimensions, Type> lhs, const Vector<Dimensions, Type>& rhs)
{
    lhs *= rhs;
    Type dotProduct = 0;
    for (const Type& component : lhs.components)
    {
        dotProduct += component;
    }
    return dotProduct;
}

/**
 *  The cross product of two Vectors is defined by:
 *      \f[
 *          \left(
 *              \begin{array}{c}
 *                  l_y r_z - l_z r_y \\
 *                  l_z r_x - l_x r_z \\
 *                  l_x r_y - l_y r_x \\
 *              \end{array}
 *          \right)
 *      \f]
 *  where
 *      \f[
 *          lhs = (l_x, l_y, l_z)
 *      \f]
 *      \f[
 *          rhs = (r_x, r_y, r_z)
 *      \f]
 *
 *  \note The cross product is only defined for three dimensional Vectors.
 */
template <typename Type>
const Vector<3, Type> Cross(const Vector<3, Type>& lhs, const Vector<3, Type>& rhs)
{
    Vector<3, Type> crossProduct;
    crossProduct.x = (lhs.y * rhs.z) - (lhs.z * rhs.y);
    crossProduct.y = (lhs.z * rhs.x) - (lhs.x * rhs.z);
    crossProduct.z = (lhs.x * rhs.y) - (lhs.y * rhs.x);
    return crossProduct;
}

#endif /* Vector_hpp */
