/**
 *  @file
 *      Matrix.hpp
 *
 *  @brief
 *      Contains a Matrix type along with related functions.
 */

#ifndef Matrix_hpp
#define Matrix_hpp

#include <iostream>

#include "Vector.hpp"

/**
 *  @brief
 *      A mathematical matrix which can be instantiated with any size or type.
 *
 *  @tparam Rows
 *      The number of rows in the Matrix.
 *
 *  @tparam Columns
 *      The number of columns in the Matrix.
 *
 *  @tparam Type
 *      The type of each element of the Matrix.
 */
template <int Rows, int Columns, typename Type>
struct Matrix;

/**
 *  @name
 *      Common Matrix types.
 */
/// @{
typedef Matrix<3, 3, float> Mat3x3f;
typedef Matrix<4, 4, float> Mat4x4f;
/// @}

/**
 *  @brief
 *      Sets the values of a Matrix's elements.
 *
 *  @param [in, out] matrix
 *      The Matrix to modify.
 *
 *  @param [in] elements
 *      The values to set.
 */
template <int Rows, int Columns, typename Type>
void SetElements(Matrix<Rows, Columns, Type>& matrix, std::initializer_list<Type> elements);

/**
 *  @brief
 *      Sets all of a Matrix's elements to a given value.
 *
 *  @param [in, out] matrix
 *      The Matrix to modify.
 *
 *  @param [in] value
 *      The value to set.
 */
template <int Rows, int Columns, typename Type>
void FillElements(Matrix<Rows, Columns, Type>& matrix, const Type& value);

/**
 *  @brief
 *      Gets a reference to a Matrix element.
 *
 *  @param [in] matrix
 *      The Matrix to query.
 *
 *  @param [in] i
 *      The row of the element.
 *
 *  @param [in] j
 *      The column of the element.
 *
 *  @returns
 *      A reference to the element at row \a i  and column \a j of \a matrix.
 */
template <int Rows, int Columns, typename Type>
Type& GetElement(Matrix<Rows, Columns, Type>& matrix, const int i, const int j);

/**
 *  @brief
 *      Gets a const reference to a Matrix element.
 *
 *  @param [in] matrix
 *      The Matrix to query.
 *
 *  @param [in] i
 *      The row of the element.
 *
 *  @param [in] j
 *      The column of the element.
 *
 *  @returns
 *      A const reference to the element at row \a i  and column \a j of \a matrix.
 */
template <int Rows, int Columns, typename Type>
const Type& GetElement(const Matrix<Rows, Columns, Type>& matrix, const int i, const int j);

/*============================================================================*/

template <int Rows, int Columns, typename Type>
struct Matrix
{
    /**
     *  @brief
     *      The Matrix components.
     *
     *  This is implemented as a union holding both a two dimensional array and
     *  an array of Vectors. This allows the Matrix to be held in contiguous
     *  memory while also allowing rows of the Matrix to be treated as Vectors.
     */
    union
    {
        /// @cond
        Type elements[Rows * Columns];
        Vector<Columns, Type> rowVectors[Rows];
        /// @endcond
    };

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] value
     *      The value to fill the Matrix with. Defaults to 0.
     *
     *  Constructs a Matrix and initializes all of its
     *  elements to \a value.
     */
    Matrix(const Type& value = 0)
    {
        FillElements(*this, value);
    }

    /**
     *  @brief
     *      Constructor.
     *
     *  @param [in] elements
     *      The values to fill the Matrix with.
     *
     *  Constructs a Matrix and initializes all of its
     *  elements with the values in \a elements.
     */
    Matrix(std::initializer_list<Type> elements)
    {
        SetElements(*this, elements);
    }

    /**
     *  @brief
     *      Subscript operator.
     *
     *  @param [in] i
     *      Row to access.
     *
     *  @return
     *      A reference to row \a i.
     */
    Vector<Columns, Type>& operator[](const int i)
    {
        return rowVectors[i];
    }

    /**
     *  @brief
     *      Const subscript operator.
     *
     *  @param [in] i
     *      Row to access.
     *
     *  @return
     *      A const reference to row \a i.
     */
    const Vector<Columns, Type>& operator[](const int i) const
    {
        return rowVectors[i];
    }

    /**
     *  @brief
     *      Gets the identity Matrix.
     *
     *  @return
     *      An identity Matrix.
     *
     *  An identity Matrix is a square Matrix which is made up of
     *  ones on the leading diagonal and zeros everywhere else.
     */
    static const Matrix Identity()
    {
        static_assert(Rows == Columns, "Identity Matrix is only defined for square Matrices");

        // Only construct the identity Matrix once, since it
        // is constant and can never change for a given size.
        static Matrix identity;
        static bool identityConstructed = false;

        if (!identityConstructed)
        {
            for (int i = 0; i < Rows; i++)
            {
                identity[i][i] = 1;
            }
            identityConstructed = true;
        }

        return identity;
    }
};

/**
 *  @brief
 *      Adds two Matrices and assigns the result to the left hand side Matrix.
 *
 *  @param [in, out] lhs
 *      The first Matrix to add and have the result assigned to.
 *
 *  @param [in] rhs
 *      The second Matrix to add.
 *
 *  @return
 *      \a lhs after the add operation.
 *
 *  Each element of \a rhs is added to the corresponding element of \a lhs.
 */
template <int Rows, int Columns, typename Type>
Matrix<Rows, Columns, Type>& operator+=(Matrix<Rows, Columns, Type>& lhs, const Matrix<Rows, Columns, Type>& rhs)
{
    for (int i = 0; i < Rows; i++)
    {
        for (int j = 0; j < Columns; j++)
        {
            lhs[i][j] += rhs[i][j];
        }
    }
    return lhs;
}

/**
 *  @brief
 *      Subtracts two Matrices and assigns the result to the left hand side Matrix.
 *
 *  @param [in, out] lhs
 *      The Matrix to subtract from and have the result assigned to.
 *
 *  @param [in] rhs
 *      The Matrix to subtract by.
 *
 *  @return
 *      \a lhs after the subtract operation.
 *
 *  Each element of \a lhs is subtracted by the corresponding element of \a rhs.
 */
template <int Rows, int Columns, typename Type>
Matrix<Rows, Columns, Type>& operator-=(Matrix<Rows, Columns, Type>& lhs, const Matrix<Rows, Columns, Type>& rhs)
{
    for (int i = 0; i < Rows; i++)
    {
        for (int j = 0; j < Columns; j++)
        {
            lhs[i][j] -= rhs[i][j];
        }
    }
    return lhs;
}

/**
 *  @brief
 *      Multiplies a Matrix by a scalar value and assigns the result to the Matrix.
 *
 *  @param [in, out] matrix
 *      The Matrix to multiply and have the result assigned to.
 *
 *  @param [in] scalar
 *      The scalar value to multiply by.
 *
 *  @return
 *      \a matrix after the multiply operation.
 *
 *  Each element of \a matrix is multiplied by \a scalar.
 */
template <int Rows, int Columns, typename Type>
Matrix<Rows, Columns, Type>& operator*=(Matrix<Rows, Columns, Type>& matrix, const Type& scalar)
{
    for (int i = 0; i < Rows; i++)
    {
        for (int j = 0; j < Columns; j++)
        {
            matrix[i][j] *= scalar;
        }
    }
    return matrix;
}

/**
 *  @brief
 *      Divides a Matrix by a scalar value and assigns the result to the Matrix.
 *
 *  @param [in, out] matrix
 *      The Matrix to divide and have the result assigned to.
 *
 *  @param [in] scalar
 *      The scalar value to divide by.
 *
 *  @return
 *      \a matrix after the divide operation.
 *
 *  Each element of \a matrix is divided by \a scalar.
 */
template <int Rows, int Columns, typename Type>
Matrix<Rows, Columns, Type>& operator/=(Matrix<Rows, Columns, Type>& matrix, const Type& scalar)
{
    for (int i = 0; i < Rows; i++)
    {
        for (int j = 0; j < Columns; j++)
        {
            matrix[i][j] /= scalar;
        }
    }
    return matrix;
}

/**
 *  @brief
 *      Adds two Matrices.
 *
 *  @param [in] lhs
 *      The first Matrix to add.
 *
 *  @param [in] rhs
 *      The second Matrix to add.
 *
 *  @return
 *      The sum of \a lhs and \a rhs.
 *
 *  The Matrices are added element-wise.
 */
template <int Rows, int Columns, typename Type>
const Matrix<Rows, Columns, Type> operator+(Matrix<Rows, Columns, Type> lhs, const Matrix<Rows, Columns, Type>& rhs)
{
    lhs += rhs;
    return lhs;
}

/**
 *  @brief
 *      Subtracts two Matrices.
 *
 *  @param [in] lhs
 *      The Matrix to subtract from.
 *
 *  @param [in] rhs
 *      The Matrix to subtract by.
 *
 *  @return
 *      \a lhs subtracted by \a rhs.
 *
 *  The Matrices are subtracted element-wise.
 */
template <int Rows, int Columns, typename Type>
const Matrix<Rows, Columns, Type> operator-(Matrix<Rows, Columns, Type> lhs, const Matrix<Rows, Columns, Type>& rhs)
{
    lhs -= rhs;
    return lhs;
}

/**
 *  @brief
 *      Multiplies a Matrix by a scalar value.
 *
 *  @param [in] matrix
 *      The Matrix to multiply.
 *
 *  @param [in] scalar
 *      The scalar value to multiply by.
 *
 *  @return
 *      \a matrix multiplied by \a scalar.
 *
 *  Each element of \a matrix is multiplied by \a scalar.
 */
template <int Rows, int Columns, typename Type>
const Matrix<Rows, Columns, Type> operator*(Matrix<Rows, Columns, Type> matrix, const Type& scalar)
{
    matrix *= scalar;
    return matrix;
}

/**
 *  @brief
 *      Multiplies a Matrix by a scalar value.
 *
 *  @param [in] scalar
 *      The scalar value to multiply by.
 *
 *  @param [in] matrix
 *      The Matrix to multiply.
 *
 *  @return
 *      \a matrix multiplied by \a scalar.
 *
 *  Each element of \a matrix is multiplied by \a scalar.
 */
template <int Rows, int Columns, typename Type>
const Matrix<Rows, Columns, Type> operator*(const Type& scalar, Matrix<Rows, Columns, Type> matrix)
{
    return matrix * scalar;
}

/**
 *  @brief
 *      Multiplies two Matrices.
 *
 *  @param [in] lhs
 *      The first Matrix to multiply.
 *
 *  @param [in] rhs
 *      The second Matrix to multiply.
 *
 *  @return
 *      The product of \a lhs and \a rhs.
 *
 *  The product of two Matrices is defined such that
 *  each element of the product has the value:
 *      \f[
 *          \sum_{k=0}^{m-1} lhs[i][k] \times rhs[k][j]
 *      \f]
 *  where \f$i\f$ is the row number of the element, \f$j\f$ is the
 *  column number and \f$m\f$ is the number of columns in \f$lhs\f$
 *  (which must be the same as the number of rows in \f$rhs\f$).
 */
template <int LHSRows, int Middle, int RHSColumns, typename Type>
const Matrix<LHSRows, RHSColumns, Type> operator*(const Matrix<LHSRows, Middle, Type>& lhs,
                                                  const Matrix<Middle, RHSColumns, Type>& rhs)
{
    Matrix<LHSRows, RHSColumns, Type> result;
    for (int i = 0; i < LHSRows; i++)
    {
        for (int j = 0; j < RHSColumns; j++)
        {
            for (int k = 0; k < Middle; k++)
            {
                result[i][j] += lhs[i][k] * rhs[k][j];
            }
        }
    }

    return result;
}

/**
 *  @brief
 *      Multiplies a Matrix by a Vector.
 *
 *  @param [in] matrix
 *      The Matrix to multiply.
 *
 *  @param [in] vector
 *      The Vector to multiply.
 *
 *  @return
 *      The product of \a matrix and \a vector.
 */
template <int Dimensions, typename Type>
const Vector<Dimensions, Type> operator*(const Matrix<Dimensions, Dimensions, Type>& matrix,
                                         const Vector<Dimensions, Type>& vector)
{
    Vector<Dimensions, Type> result;
    for (int i = 0; i < Dimensions; i++)
    {
        for (int j = 0; j < Dimensions; j++)
        {
            result[i] += matrix[i][j] * vector[j];
        }
    }

    return result;
}

/**
 *  @brief
 *      Divides a Matrix by a scalar value.
 *
 *  @param [in] matrix
 *      The Matrix to divide.
 *
 *  @param [in] scalar
 *      The scalar value to divide by.
 *
 *  @return
 *      \a matrix divided by \a scalar.
 *
 *  Each element of \a matrix is divided by \a scalar.
 */
template <int Rows, int Columns, typename Type>
const Matrix<Rows, Columns, Type> operator/(Matrix<Rows, Columns, Type> matrix, const Type& scalar)
{
    matrix /= scalar;
    return matrix;
}

/**
 *  @brief
 *      Checks to see if two Matrices are equal.
 *
 *  @param [in] lhs
 *      The first Matrix to compare.
 *
 *  @param [in] rhs
 *      The second Matrix to compare.
 *
 *  @retval true
 *      \a lhs and \a rhs are equal.
 *
 *  @retval false
 *      \a lhs and \a rhs are not equal.
 *
 *  Two Matrices are equal if and only if each of their elements are equal.
 */
template <int Rows, int Columns, typename Type>
bool operator==(const Matrix<Rows, Columns, Type>& lhs, const Matrix<Rows, Columns, Type>& rhs)
{
    return memcmp(lhs.elements, rhs.elements, sizeof(lhs)) == 0;
}

/**
 *  @brief
 *      Checks to see if two Matrices are not equal.
 *
 *  @param [in] lhs
 *      The first Matrix to compare.
 *
 *  @param [in] rhs
 *      The second Matrix to compare.
 *
 *  @retval true
 *      \a lhs and \a rhs are not equal.
 *
 *  @retval false
 *      \a lhs and \a rhs are equal.
 *
 *  Two Matrices are not equal if any their elements are not equal.
 *
 *  This is the inverse of operator==, and is defined as such.
 */
template <int Rows, int Columns, typename Type>
bool operator!=(const Matrix<Rows, Columns, Type>& lhs, const Matrix<Rows, Columns, Type>& rhs)
{
    return !(lhs == rhs);
}

/**
 *  @brief
 *      Inserts a Matrix into an output stream.
 *
 *  @param [in, out] stream
 *      The output stream.
 *
 *  @param [in] matrix
 *      The Matrix to insert.
 *
 *  @return
 *      \a stream.
 */
template <int Rows, int Columns, typename Type>
std::ostream& operator<<(std::ostream& stream, const Matrix<Rows, Columns, Type>& matrix)
{
    stream << "(" << matrix.rowVectors[0];
    for (int i = 1; i < Rows; i++)
    {
        stream << ", " << matrix.rowVectors[i];
    }
    stream << ")";

    return stream;
}

/**
 *  Assigns \a elements to \a matrix.elements.
 */
template <int Rows, int Columns, typename Type>
void SetElements(Matrix<Rows, Columns, Type>& matrix, std::initializer_list<Type> elements)
{
    KI_ASSERT(elements.size() == Rows * Columns,
              elements.size() << " elements was provided; expected " << Rows * Columns);

    int i = 0;
    for (Type element : elements)
    {
        matrix.elements[i++] = element;
    }
}

/**
 *  Fills \a matrix.elements with \a value.
 */
template <int Rows, int Columns, typename Type>
void FillElements(Matrix<Rows, Columns, Type>& matrix, const Type& value)
{
    for (int i = 0; i < Rows; i++)
    {
        for (int j = 0; j < Columns; j++)
        {
            matrix[i][j] = value;
        }
    }
}

/**
 *  Accesses \a matrix.elements at \a Rows * \a i + \a j.
 */
template <int Rows, int Columns, typename Type>
Type& GetElement(Matrix<Rows, Columns, Type>& matrix, const int i, const int j)
{
    return matrix.elements[Rows * i + j];
}

/**
 *  Accesses \a matrix.elements at \a Rows * \a i + \a j.
 */
template <int Rows, int Columns, typename Type>
const Type& GetElement(const Matrix<Rows, Columns, Type>& matrix, const int i, const int j)
{
    return matrix.elements[Rows * i + j];
}

#endif /* Matrix_hpp */
